#!/usr/bin/env python3
import subprocess, os.path, sys, argparse, time, struct


Description = """
Tool to create a Latex table containing the compression ratio of the 
stock market indexer compared with gz and xz.
Test simultaneously a given set of files on a combinations of window sizes, 
block sizes, and encoders"""

Latex_name = {'csa':"\\csa", 'smi':"\\smi",
              'lsd':"\\lsd", 'lsk':"\\lsk", 'dlt':"\\dlt",
              'mix':"\\mix", 'mid':"\\mixd",
              'gzip':"\\gzip", 'xz':"\\xz"}
               
# compare the compression ratio of delta compressors
def mydelta_test(wsize,bsize,files,basedir,encs,check_decompression,python_dec,strict):
  # init extensions for strict/non-strict compression 
  if strict:
    _check = "scheck"; _strict = "_strict"; 
    _dext = "ez"; _pdext = " E_EXT=ezd"; _xo = "yo" 
  else:
    _check = "check";  _strict = "";       
    _dext = "dz"; _pdext = " D_EXT=dzd"; _xo = "xo"
  # init storage
  csa = []
  csa_string = "%-7s $q\\!=\\!%s, B\\!=\\!%s$" % (Latex_name['csa'],wsize,bsize)
  delta = {}; delta_string= {}
  for e in encs:
    delta[e] = []
    delta_string[e] = "%-7s  %2s %2s" % (Latex_name[e],"","")
  # process all test files
  for f in files:
    # for each file consider all encoders
    for enc in encs:
      fname = os.path.join(basedir,f)
      if check_decompression:
        command = "make index_%s BASE=%s WSIZE=%s BSIZE=%s ENC=%s" % (_check,fname,wsize,bsize,enc)
        if python_dec: command += _pdext
      else:
        command = "make index%s BASE=%s WSIZE=%s BSIZE=%s ENC=%s" % (_strict,fname,wsize,bsize,enc)
      subprocess.check_call(command.split(),stdout=sys.stderr)
      if check_decompression:
        checksize = os.path.getsize(fname+".%s.%s.%s.%s" % (enc,wsize,bsize,_check))
        assert checksize==0, "ERROR: file %s.%s.%s does not check" % (f,wsize,bsize)
      fsize = os.path.getsize(fname)
      fd = os.path.getsize(fname+".%s.%s.%s.%s" % (enc,wsize,bsize,_dext))
      fo = os.path.getsize(fname+".%s.%s.%s.csa" % (wsize,_xo,bsize))
      delta[enc].append(fd/fsize)
    csa.append(fo/fsize)
  assert len(csa)==len(files)    
  for i in range(len(csa)):
    csa_string += " & %-5.2f" % csa[i]
    for e in encs:
      delta_string[e] += " & %-5.2f" % delta[e][i]
  out = ""
  out += csa_string + " \\\\\\hline\n"
  for e in encs:
    out +=  delta_string[e] + " \\\\\\hline\n"
  out = out[:-1] + "\\hline\n"
  return out  



# compare compression ratio for different window/block size
# for a single encoder. only report the total size of the smi
def myalgo_test(wsize,bsize,files,basedir,enc,check_decompression,python_dec,strict):
  # init extensions for strict/non-strict compression 
  if strict:
    _check = "scheck"; _strict = "_strict"; 
    _dext = "ez"; _pdext = " E_EXT=ezd"; _xo = "yo" 
  else:
    _check = "check";  _strict = "";       
    _dext = "dz"; _pdext = " D_EXT=dzd"; _xo = "xo"
  # process all test files
  delta = []
  csa = []
  for f in files:
    fname = os.path.join(basedir,f)
    if check_decompression:
      command = "make index_%s BASE=%s WSIZE=%s BSIZE=%s ENC=%s" % (_check,fname,wsize,bsize,enc)
      if python_dec: command += _pdext
    else:
      command = "make index%s BASE=%s WSIZE=%s BSIZE=%s ENC=%s" % (_strict,fname,wsize,bsize,enc)
    subprocess.check_call(command.split(),stdout=sys.stderr)
    if check_decompression:
      checksize = os.path.getsize(fname+".%s.%s.%s.%s" % (enc,wsize,bsize,_check))
      assert checksize==0, "ERROR: file %s.%s.%s does not check" % (f,wsize,bsize)
    fsize = os.path.getsize(fname)
    fd = os.path.getsize(fname+".%s.%s.%s.%s" % (enc,wsize,bsize,_dext))
    fo = os.path.getsize(fname+".%s.%s.%s.csa" % (wsize,_xo,bsize))
    delta.append(fd/fsize)
    csa.append(fo/fsize)
  # t = "%-7s  %2s %2s" % (Latex_name['smi']+"-"+Latex_name[enc],wsize,bsize)
  t = "%-5s $q\\!=\\!%s, B\\!=\\!%s$" % (Latex_name['smi'],wsize,bsize)
  for i in range(len(delta)):
    t += " & %-5.2f" % (delta[i]+csa[i])
  t += " \\\\\\hline\n"
  return t  





# we use the fact that files have been already compressed (by the makefile)
def make_zip(model,files,basedir):
  x = model.split("&")
  c1 = len(x[0])
  c2 = len(x[1])
  # process all test files
  gz = []
  xz = []
  for f in files:
    fname = os.path.join(basedir,f)
    #command = "make index BASE=%s WSIZE=%s BSIZE=%s" % (fname,wsize,bsize)
    #subprocess.check_call(command.split())
    fsize = os.path.getsize(fname)
    fd = os.path.getsize(fname+".gz")
    fo = os.path.getsize(fname+".xz")
    gz.append(fd/fsize)
    xz.append(fo/fsize)
  d = "%-*s" % (c1, Latex_name['gzip']) 
  o = "%-*s" % (c1, Latex_name['xz']) 
  for i in range(len(gz)):
    d += "& %-*.2f" % (c2-1,gz[i])  
    o += "& %-*.2f" % (c2-1,xz[i])  
  d += "\\\\\\hline\n"
  o += "\\\\\\hline\\hline\n"
  return d+o

def make_names(model,files):
  x = model.split("&")
  c1 = len(x[0])
  c2 = len(x[1])
  row = " "*c1
  for f in files:
    row += "& %-*s" % (c2-1,f)
  row = row + "\\\\\\hline\\hline\n"
  return row
  
# check that all files in flist exist in directory basename
def check_files(flist, basename):
  for f in flist:
    fname = os.path.join(basename,f)
    assert os.path.exists(fname), "Unknown test file: " + fname

#char that all encoders are known, ie exist in Latex_name dictionary
def check_encoders(encoders):
  for e in encoders:
    if not e in Latex_name:
      assert False, "Unknown encoder: " + e

def main():
  # show_command_line(sys.stderr)
  parser = argparse.ArgumentParser(description=Description, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('files', help='test files separated by colons', type=str)
  parser.add_argument('-w', '--window_sizes', help='sizes of sliding window separated by colons (def 4:8)', default='4:8', type=str)  
  parser.add_argument('-b', '--block_sizes', help='sizes of block separated by colons (def 32:64)', default='32:64', type=str)  
  parser.add_argument('-d', '--base_dir', help='base directory (def .)', default='.', type=str)
  parser.add_argument('-e', '--encs', help='encoders separated by colons (def lsk)', default='lsk', type=str)
  parser.add_argument('-c', '--check', help='check decompression (def No)',action='store_true')
  parser.add_argument('-p', '--pythondec', help='use python slow decompressor (def No)',action='store_true')
  parser.add_argument('-s', '--strict', help='use strict mode for (de)compression (def No)',action='store_true')
  args = parser.parse_args()
  # run the algorithm   
  s1 = time.time()
  ifiles = args.files.split(":")
  wsizes = args.window_sizes.split(":")
  bsizes = args.block_sizes.split(":")
  encoders = args.encs.split(":")  
  check_files(ifiles,args.base_dir)
  check_encoders(encoders)
  table = ""
  for b in bsizes:
    for w in wsizes:
      if len(encoders)>1:
        r = mydelta_test(w,b,ifiles,args.base_dir,encoders,args.check,args.pythondec,args.strict)
      else:
        r = myalgo_test(w,b,ifiles,args.base_dir,encoders[0],args.check,args.pythondec,args.strict)
      table += r
  # the model row is the first row up to the "\\" sequence    
  model_row = table.split("\n")[0].split("\\\\")[0]
  # create row with name of the test files
  names = make_names(model_row, ifiles)
  # add rows for gzip and xz
  table = names + table + make_zip(model_row,ifiles,args.base_dir) 
  e1 = time.time()
  print("Elapsed time: %.3f\n" % (e1-s1),file=sys.stderr)
  print(table)
  exit(0)

  
if __name__ == '__main__':
    main()
 
