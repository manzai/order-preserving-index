#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <xmmintrin.h>
#include <sdsl/suffix_arrays.hpp>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace sdsl;
using namespace std;

// Giovanni: deleted many unused functions and global variables
// transformed all text and pattern symbols to type int. 
// Now text and pattern symbols can be in the range -2^31 to 2^31-1

// The core of the algorithm (prep an exec) was not changed 

#ifndef CHARTYPE
#define CHARTYPE unsigned char
#endif
// maximum pattern length
#define MAXPAT  256


static struct
{
  int patlen;
  int pat[MAXPAT];
} pat;


CHARTYPE pattern1[MAXPAT];
int patlength=0,textlength=0;
int sort_p[MAXPAT],r[MAXPAT];
CHARTYPE sort_abc[MAXPAT];
csa_wt<> fm_index;


void prep(int *y, unsigned n, int *pattern, register int m)
{
  int i=0,j,b;
  int temp,temp1;
  static int initialized = 0;

  //printf("m is:%d\n",m);
  /*for (i = 0; i < n; i++)
  {
    printf("y[%d]=%d\n",i,y[i]);

  }*/
  if (!initialized) {
    /* build the index */
    
    cerr << "Building the index\n";

    initialized = 1;

    //CHARTYPE text[n+pat.patlen+2];
    CHARTYPE *text = (CHARTYPE *)malloc(n+pat.patlen+2);
    //int z=0,i=0,j=0,k=0,temp=0, w=0;
    // int match=0, sum=0,b;
    int j=0;
    textlength=0;
    // int m=pat.patlen;
    for (unsigned i = 0; i < n-1; i++)
    {
      if(y[i]<y[i+1])
        text[j]='a';
      else 
        text[j]='b';
      j++;
      textlength++;
    }
    //  cout<<text<<endl;;
    std::string str((char *)&text[0], n+pat.patlen+2);
    construct_im(fm_index, str.c_str(), 1);
  }

  pat.patlen = m;
  j=0;
  patlength=0;
  memcpy(pat.pat, pattern,  m*sizeof(unsigned));
  memcpy(sort_p,pattern, m*sizeof(unsigned)); 
  for (i = 0; i < m-1; i++) {
    if(pat.pat[i]<pat.pat[i+1])
      pattern1[j]='a';      //pattern1 is the pattern in the form of abc
    else 
       pattern1[j]='b';
    j++;
    patlength++;
  }
  pattern1[j]=0;
 //cout<<"Pattern1 is:"<<pattern1<<endl;
  for(i=0;i<m;i++)
  r[i]=i;
  for(i=1;i< m;i++) {
    for(j=0;j< m-1;j++)
      if(sort_p[j]>sort_p[j+1])   //sort_p is the sorted array of pattern
          {
            temp=sort_p[j];  
            sort_p[j]=sort_p[j+1];
            sort_p[j+1]=temp;
            temp1=r[j];
            r[j]=r[j+1];
            r[j+1]=temp1;
          }                
  }
  b=0;      
  for(i=0;i<m-1;i++)
  {
    if(pat.pat[r[i]]< pat.pat[r[i+1]])
      sort_abc[b]='a';    //sort_abc is the abc coding of the sorted T array
    else 
      sort_abc[b]='b';
    b++;
  }

} 

int exec(int *y, register int n) 
{  
//  CHARTYPE text[n+pat.patlen+2];
  
  (void) n;
  int i=0,j=0,k=0,w=0;
  int total_match=0, matches=0, sum=0,b;
  int m=pat.patlen;
  CHARTYPE x[MAXPAT]; // Giovanni: was LIM2 (== 600000)
  j=0;

  //cout << "-------------\nSearching:\n";
  //for(int i=0;i<m;i++)
  //  cout << pat.pat[i] << " vs " << pattern1[i] << endl;

  matches=count(fm_index, (const char*)pattern1);
  auto locations= locate(fm_index,(const char*)pattern1);

  //cout<<"no. of matches are:"<<matches <<endl;
  w = locations[k];  
  while(k < matches ) {
    //  printf("\nloc is:%d\n",w);
    sum=0,b=0;
    for(i=0;i<m-1;i++) {
      //  printf("y[%d]=%d   y[%d]=%d\n",w+r[i],y[w+r[i]],w+r[i+1] ,y[w+r[i+1]]);
      if(y[w+r[i]]< y[w+r[i+1]])  
          x[b]='a'; // x is the text in the form of abc coding relative to r 
      else if(y[w+r[i]] > y[w+r[i+1]])  
          x[b]='c';
      else
        x[b]='b';
      b++;
    }
    b=0;
    for(j=0;j< m-1;j++) {
      if(x[j]==sort_abc[j])
        sum++;
    }
    if(sum==m-1) {
  /*    for(i=0;i<m-1;i++)
      {printf("Verification\n");
    printf("y[%d]=%d   y[%d]=%d\n",w+r[i],y[w+r[i]],w+r[i+1] ,y[w+r[i+1]]);        
      }*/
      total_match++;
    }
    k++;
    w = locations[k];
  }
  return(total_match);
}
