/* fmop_main.c
 * execute the fm-opm algorithm for the order-preserving pattern matching problem
 * see:
 *   Tamanna Chhabra, M. Oguzhan Külekci, and Jorma Tarhio
 *   Alternative Algorithms for Order-Preserving Matching
 *   Stringology, 2015.
 *   Url: http://www.stringology.org/event/2015/p05.html
 * 
 * Note: to compile this file you must first install the sdsl-lite library
 *       from https://github.com/simongog/sdsl-lite
 *    
 * The code in FM_OPM.cpp is by Tamanna Chhabra and Jorma Tarhio
 * fmop_main.c is by Giovanni Manzini 
 * */ 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <assert.h>

// special value used to mark the end of a pattern
// must not be found in a text or pattern file
#define EOP (INT32_MIN)


void prep(int *y, unsigned n, int *pattern, register int m);
int exec(int *y, register int n);



void die(const char *s)
{
  perror(s);
  exit(1);
}    


// read an array of ints from file 
int32_t *read_text(char *name, int *tlen)
{
  FILE *fin = fopen(name,"rb");
  if(fin==NULL) die("file open");
  // get file size
  if(fseek(fin,0,SEEK_END)!=0) die("fseek");
  long n = ftell(fin);
  if(n%4!=0) die("Invalid input file");
  n = n/4;  // size in bytes
  // ------ allocate and read text file
  int32_t *text = (int32_t *) malloc(4*n); // for some reason we have to skip one char 
  if(text==NULL) die("malloc 1");
  rewind(fin);
  for(int i=0;i<n;i++) {
    int32_t v;
    size_t s = fread(&v,4,1,fin);
    if(s!=1) {
      fprintf(stderr,"Input file read error n=%d, s=%zu\n",i,s); exit(1);
    }
    if(v == EOP) {
      fprintf(stderr, "Invalid value (%d). Exiting\n",v); exit(1);
    }
    text[i] = v; 
  }
  fclose(fin);
  *tlen = n;
  return text;
}


int *read_patterns(const char *name, int *np)
{
  FILE *fin = fopen(name,"rb");
  if(fin==NULL) die("file open");
  int *patterns = NULL;
  int plen=0, psize=0, delta=10000;
  char *buf = NULL;
  size_t buflen = 0; 
  int numpat=0;
  
  // read patterns, one per line
  while(true) {
    int e = getline(&buf, &buflen, fin);
    if(e<=1) break; // no more patterns
    // read values 
    char *s = strtok(buf," \n");
    while(1) {
      int v = EOP; // terminator value
      if(s!=NULL) {
        e = sscanf(s,"%d",&v);
        if(e!=1) die("Illegal pattern file");
        if(v==EOP) die("Illegal value in pattern file");
      }
      // add v to pattern
      if(plen>=psize) {
        assert(plen==psize);
        psize += delta;
        patterns = (int *) realloc(patterns,psize*sizeof(int));
        if(patterns==NULL) die("End of memory");
      }
      assert(plen<psize);
      patterns[plen++] = v;
      if(s==NULL) break;
      s= strtok(NULL," \n");
    }
    assert(patterns[plen-1]==EOP);
    numpat++;
  }
  *np = numpat;
  return patterns;
}
  

static void print_help(char *name)
{
  printf("Usage: %s text_file pattern_file [-hv]\n\n", name);
  puts("    OP-search inside text_file of the patterns in pattern_file");
  puts("      using the FM-OPM algorithm by T. Chhabra et. al.\n");
  puts("    text_file must be in binary: each symbol stored as a signed 32 bit integer\n");
  puts("    pattern_file must be in ascii: each pattern stored in a single line");
  puts("      with numerical values separated by the space char\n");
  puts("    Text and pattern symbols must be in the range [-2**31+1,2**31-1]\n"); 
  puts("    Options:");
  puts("      -h   This help message");
  puts("      -v   Verbose: write searched patterns and # occurrences to stderr\n");
  exit(1);
}


int main(int argc, char** argv) {
  int c;
  bool verbose = false;
  // ----------- parse options 
  while ((c = getopt (argc, argv, "vh")) != -1)
    switch (c) {
      case 'v':
        verbose = true;
        break;
      case 'h':
      default:
        print_help(argv[0]);
    }
  // --- check mandatory args are there 
  argc -= (optind -1);  // get rid of optional arguments  
  if(argc!=3 && argc!=4) 
    print_help(argv[0]);
  argv += optind-1;
  // read sequence data
  int tlen;
  int32_t *text = read_text(argv[1],&tlen);
  fprintf(stderr,"Input file contains %d values\n",tlen);
  int pnum;
  int *pat = read_patterns(argv[2],&pnum);
  fprintf(stderr,"Pattern file contains %d patterns\n",pnum);
  // force index creation 
  {
    int dummy[] = {1,2};
    prep(text, tlen, dummy,2);
  }
  // main loop
  int64_t tot_found = 0;
  clock_t t=clock();
  int *p = pat;
  for(int i=0;i<pnum;i++) {
    int len=0;
    int *s=p;
    while(*s!=EOP) {
      s++;
      len++;
    }
    if(len==0) die("Empty pattern");
    // preprocessing
    prep(text, tlen, p,len);
    // search
    int tot = exec(text,tlen);
    tot_found += tot;
    // if verbose print # of occurrence and pattern (make timing unreliable)
    if(verbose) {
      fprintf(stderr,"Occ: %-8d", tot);
      for(int i=0;i<len;i++) fprintf(stderr," %d", p[i]);
      fprintf(stderr,"\n");
    }
    // proceed to next pattern
    p=s+1;
  }
  t = clock() - t;
  printf("%ld OP matches found. Elapsed ",tot_found);
  printf("%lf seconds ", ((double) t)/CLOCKS_PER_SEC);
  printf("%lf millisecXpat\n", (1000.0/pnum)*((double) t)/CLOCKS_PER_SEC);
  free(pat);
  free(text);
  return 0;
}

