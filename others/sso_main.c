/* sso_main.c
 * execute the sso4 algorithm for the order-preserving pattern matching problem
 * see:
 *   Tamanna Chhabra, Simone Faro, M. Oguzhan Külekci, and Jorma Tarhio
 *   Engineering order-preserving pattern matching with SIMD parallelism.
 *   Software: Practice and Experience, 2016.
 *   DOI: 10.1002/spe.2433
 *   
 * The code in sso4.c is by Tamanna Chhabra and Jorma Tarhio
 * sso4_main.c is by Giovanni Manzini 
 * */ 
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <assert.h>
#include "sso4.c"

// special value used to mark the end of a pattern
// must not be found in a text or pattern file
#define EOP (-128)


void die(const char *s)
{
  perror(s);
  exit(1);
}    

// read an array of char from file 
char *read_text(char *name, int *tlen)
{
  FILE *fin = fopen(name,"rb");
  if(fin==NULL) die("file open");
  // get file size
  if(fseek(fin,0,SEEK_END)!=0) die("fseek");
  long n = ftell(fin);
  if(n%4!=0) die("Invalid input file");
  n = n/4;  // size in bytes
  // ------ allocate and read text file
  char *text = malloc(n+1); // for some reason we have to skip one char 
  if(text==NULL) die("malloc 1");
  rewind(fin);
  for(int i=0;i<n;i++) {
    int32_t v;
    size_t s = fread(&v,4,1,fin);
    if(s!=1) {
      fprintf(stderr,"Input file read error n=%d, s=%zu\n",i,s); exit(1);
    }
    if(v<-127 || v>127) {
      fprintf(stderr, "Invalid value (%d). Exiting\n",v); exit(1);
    }
    text[1+i] = (char) v; // +1 for legacy reasons 
  }
  fclose(fin);
  *tlen = n;
  return text;
}


char *read_patterns(const char *name, int *np)
{
  FILE *fin = fopen(name,"rb");
  if(fin==NULL) die("file open");
  char *patterns = NULL;
  int plen=0, psize=0, delta=10000;
  char *buf = NULL;
  size_t buflen = 0; 
  int numpat=0;
  
  // read patterns, one per line
  while(true) {
    int e = getline(&buf, &buflen, fin);
    if(e<=1) break; // no more patterns
    // read values 
    char *s = strtok(buf," \n");
    while(1) {
      int v = EOP; // terminator value
      if(s!=NULL) {
        e = sscanf(s,"%d",&v);
        if(e!=1) die("Illegal pattern file");
        if(v<-127 || v > 127) die("Illegal value in pattern file");
      }
      // add v to pattern
      if(plen>=psize) {
        assert(plen==psize);
        psize += delta;
        patterns = realloc(patterns,psize);
        if(patterns==NULL) die("End of memory");
      }
      assert(plen<psize);
      patterns[plen++] = v;
      if(s==NULL) break;
      s= strtok(NULL," \n");
    }
    assert(patterns[plen-1]==EOP);
    numpat++;
  }
  *np = numpat;
  return patterns;
}
  

static void print_help(char *name)
{
  printf("Usage: %s text_file pattern_file [-hv]\n\n", name);
  puts("    OP-search inside text_file of the patterns in pattern_file");
  puts("      using the SIMD_OPPM algorithm by T. Chhabra et. al.\n");
  puts("    text_file must be in binary: each symbol stored as a signed 32 bit integer\n");
  puts("    pattern_file must be in ascii: each pattern stored in a single line");
  puts("      with numerical values separated by the space char\n");
  puts("    Text and pattern symbols must be in the range [-127,127]\n"); 
  puts("    Options:");
  puts("      -h   This help message");
  puts("      -v   Verbose: write searched patterns and # occurrences to stderr\n");
  exit(1);
}


int main(int argc, char** argv) {
  int c;
  bool verbose = false;
  // ----------- parse options 
  while ((c = getopt (argc, argv, "vh")) != -1)
    switch (c) {
      case 'v':
        verbose = true;
        break;
      case 'h':
      default:
        print_help(argv[0]);
    }
  // --- check mandatory args are there 
  argc -= (optind -1);  // get rid of optional arguments  
  if(argc!=3 && argc!=4) 
    print_help(argv[0]);
  argv += optind-1;
  // read sequence data
  int tlen;
  char *text = read_text(argv[1],&tlen);
  fprintf(stderr,"Input file contains %d values\n",tlen);
  int pnum;
  char *pat = read_patterns(argv[2],&pnum);
  fprintf(stderr,"Pattern file contains %d patterns\n",pnum);
  // main loop
  int64_t tot_found = 0;
  clock_t t=clock();
  char *p = pat;
  for(int i=0;i<pnum;i++) {
    int len=0;
    char *s=p;
    while(*s!=EOP) {
      s++;
      len++;
    }
    if(len==0) die("Empty pattern");
    if(len>MAXPAT) {
      fprintf(stderr,"Pattern too long. Current limit: %d. Change MAXPAT and recompile\n",MAXPAT);
      exit(1);
    }    
    // preprocessing
    prep(p,len);
    // search
    int tot = exec(text+1,tlen);     // text+1 for legacy reasons
    tot_found += tot;
    // if verbose print # of occurrence and pattern (make timing unreliable)
    if(verbose) {
      fprintf(stderr,"Occ: %-8d", tot);
      for(int i=0;i<len;i++) fprintf(stderr," %d", p[i]);
      fprintf(stderr,"\n");
    }
    // proceed to next
    p=s+1;
  }
  t = clock() - t;
  printf("%ld OP matches found. Elapsed ",tot_found);
  printf("%lf seconds ", ((double) t)/CLOCKS_PER_SEC);
  printf("%lf millisecXpat\n", (1000.0/pnum)*((double) t)/CLOCKS_PER_SEC);
  free(pat);
  free(text);
  return 0;
}
