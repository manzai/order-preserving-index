#include <xmmintrin.h>
#include <immintrin.h>
#include <x86intrin.h>

#define LIM 65536
#define MAXPAT  256
#define DATATYPE  char
// giovanni: since DATATYPE is char the range for text 
// and pattern is [-128,127]

static struct
{
        int patlen;
        DATATYPE pat[MAXPAT];
} pat;

int patlength=0,textlength=0;
static int popc[LIM];
DATATYPE T[MAXPAT];
int r[MAXPAT];
int eq[MAXPAT];


// preprocessing of the pattern
void prep(const DATATYPE *pattern, register int m)  // made changes to the main also  
{
  pat.patlen = m;
  int i,x,j=0;
  DATATYPE temp;
  int temp1;

  // For some reason doing this check here makes the program at least 10% slower
  // test moved imemdiately before calling prep()  
  // if(m>MAXPAT) {
  //   fprintf(stderr,"Pattern too long. Current limit: %d. Change MAXPAT in %s and recompile\n",MAXPAT,__FILE__);
  //   exit(1);
  // }

  memcpy(pat.pat, pattern, sizeof(DATATYPE)*m);
  memcpy(T,pattern,m*sizeof(DATATYPE));

  for(i=0;i<m;i++)      // auxiliary table 'r' for checking
    r[i]=i;

   for(i=0;i<m;i++)     // auxiliary table 'eq' for equalities
        eq[i]=0;
        
   for(i=1;i< m;i++) { 
      for(j=0;j< m-1;j++)
        if(T[j]>T[j+1])   //T is the sorted array of pattern
        {
          temp=T[j];  
          T[j]=T[j+1];                                                    
          T[j+1]=temp;
          temp1=r[j];
          r[j]=r[j+1];
          r[j+1]=temp1;
        }
  }

  for(j=0;j< m;j++)
  {
    if(pat.pat[r[j]]==pat.pat[r[j+1]])
      eq[j]=1;
  } 
    

  for(j=0;j< LIM;j++) {
    x = j;
    x -=  (x>>1) & 0x5555U;
    x  = ((x>>2) & 0x3333U) + (x & 0x3333U);
    x  = ((x>>4) + x) & 0x0f0fU;
    popc[j]  = ((x>>8) + x) & 0x00ffU;
  }
}


int exec(const DATATYPE *y, register int n)
{
  int i=0,j,matches=0;
  int m=pat.patlen-1;
  uint16_t k;
  __m128i x_ptr, y_ptr;

  for (j = 0; j < n; j=j+16) 
  {
    k = ~(uint16_t) 0;
    x_ptr = _mm_loadu_si128(( __m128i *)(y+r[0]));      

//----------------------
    y_ptr = _mm_loadu_si128(( __m128i *)(y+r[1]));
    if(eq[0])
        k &=_mm_movemask_epi8( _mm_cmpeq_epi8(y_ptr , x_ptr) );
    else
        k &=_mm_movemask_epi8( _mm_cmpgt_epi8(y_ptr , x_ptr) );
    x_ptr = y_ptr;
    y_ptr = _mm_loadu_si128(( __m128i *)(y+r[2]));
    if(eq[1])
        k &=_mm_movemask_epi8( _mm_cmpeq_epi8(y_ptr , x_ptr) );
    else
        k &=_mm_movemask_epi8( _mm_cmpgt_epi8(y_ptr , x_ptr) );
    x_ptr = y_ptr;
    y_ptr = _mm_loadu_si128(( __m128i *)(y+r[3]));
    if(eq[2])
        k &=_mm_movemask_epi8( _mm_cmpeq_epi8(y_ptr , x_ptr) );
    else
        k &=_mm_movemask_epi8( _mm_cmpgt_epi8(y_ptr , x_ptr) );
    x_ptr = y_ptr;
    y_ptr = _mm_loadu_si128(( __m128i *)(y+r[4]));
    if(eq[3])
        k &=_mm_movemask_epi8( _mm_cmpeq_epi8(y_ptr , x_ptr) );
    else
        k &=_mm_movemask_epi8( _mm_cmpgt_epi8(y_ptr , x_ptr) );
//----------------------

    if(k==0) goto out;
    x_ptr = y_ptr;
    for (i = 4; i < m; i++)
    {
      y_ptr = _mm_loadu_si128(( __m128i *)(y+r[i+1]));
      if(eq[i])
        k &=_mm_movemask_epi8( _mm_cmpeq_epi8(y_ptr , x_ptr) );
      else
        k &=_mm_movemask_epi8( _mm_cmpgt_epi8(y_ptr , x_ptr) );
      x_ptr = y_ptr;

    if(k==0) goto out;
    }

   matches+=popc[k]; // something was found
   out: y=y+16;   

  }
  return(matches);
}

