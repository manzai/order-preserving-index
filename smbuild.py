#!/usr/bin/env python3
# smbuild.py
#
# Tool build a stock market index given the raw data
# the window size, the block size, and the encoder 
# This script simply calls the necessary tools in the right sequence
#
# Copyright 2018 Giovanni Manzini
import os, os.path, sys, argparse, time, subprocess


Description = """
Tool to build a compressed index for the order preserving pattern matching problem"""

# available encoders for the delta component 
Encoders = ['lsk','dlt','lsd','mix','mid']
Enc_list = "|".join(Encoders)

strict_split_name = "eqsplit.py"
strict_split_ext = ".%d.yo"
strict_split_exd = ".%d.yd"
strict_delta_ext = "ez"
nostrict_split_name = "xosplit.py"
nostrict_split_ext = ".%d.xo"
nostrict_split_exd = ".%d.xd"
nostrict_delta_ext = "dz"
csa_ext = ".%d.csa"
makecsa_ename = "makecsa.%d.x"
delta_ename = "deltacomp.py"


def main():
  parser = argparse.ArgumentParser(description=Description, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('input', help='input file', type=str)
  parser.add_argument('--wsize', '-w', help='window size (def 6)', default=6, type=int)  
  parser.add_argument('--bsize', '-b', help='size of a block in the compressed index (def 32)', default=32, type=int)  
  parser.add_argument('--encoder', '-e', help='encoder one of: %s (def lsk)' % Enc_list, default='lsk', type=str)  
  parser.add_argument('--strict', '-s', help='strict mode (default No)', action='store_true')
  # parser.add_argument('--nostrict', '-n', help='non-strict mode', action='store_true')
  parser.add_argument('--keep', '-k',  help='keep temporary files (default No)',action='store_true')
  args = parser.parse_args()
  check_input(args)
  # define mode
  #strict_mode = not(args.nostrict) 
  strict_mode = args.strict 
  # ----- file names creation
  # path for the executables
  exe_path = os.path.split(sys.argv[0])[0]
  if strict_mode:
    split_exe = os.path.join(exe_path,strict_split_name) 
    split_ext = strict_split_ext % args.wsize
    split_exd = strict_split_exd % args.wsize
    delta_ext = strict_delta_ext
  else:   
    split_exe = os.path.join(exe_path,nostrict_split_name)
    split_ext = nostrict_split_ext % args.wsize
    split_exd = nostrict_split_exd % args.wsize
    delta_ext = nostrict_delta_ext
  # name of the compressed index file   
  index_name = args.input+split_ext + (csa_ext % args.bsize)
  # name of the compressed delta file
  delta_name = "{name}.{enc}.{w}.{b}.{ext}".format(name=args.input,
                enc=args.encoder,w=args.wsize,b=args.bsize,ext=delta_ext) 

  # ---- opening log file 
  logfile_name = args.input + ".log"
  print("Sending logging messages to file:", logfile_name)
  with open(logfile_name,"a") as logfile:  
    
    # ---------- splitting into order/delta components 
    start0 = start = time.time()       
    command = "{exe} {infile} -w {wsize}".format(exe=split_exe, 
                                    wsize=args.wsize, infile=args.input)
    print("==== Splitting input. Command:", command)
    if(execute_command(command,logfile,logfile_name)!=True):
      sys.exit(10)
    print("Elapsed time: {0:.4f}".format(time.time()-start))

    # ------- index construction 
    start = time.time()
    command = "{exe} {infile}".format(exe=os.path.join(exe_path,makecsa_ename % args.bsize), 
                               infile=args.input+split_ext)
    print("==== Make index. Command:", command)
    if(execute_command(command,logfile,logfile_name)!=True):
      sys.exit(20)
    print("Elapsed time: {0:.4f}".format(time.time()-start))

    # ------- delta compression 
    start = time.time()
    command = "{exe} {infile1} {infile2} -b {bsize} -e {enc}".format(
              exe=os.path.join(exe_path,delta_ename), 
              infile1=args.input, infile2=args.input+split_ext,
              bsize = args.bsize, enc=args.encoder)
    if strict_mode: command += " --strict"
    print("==== Delta compression. Command:", command)
    if(execute_command(command,logfile,logfile_name)!=True):
      sys.exit(30)
    print("Elapsed time: {0:.4f}".format(time.time()-start))
    print("Total construction time: {0:.4f}".format(time.time()-start0))

    # -------- delete temporary files 
    if not args.keep:
      try:
        os.remove(args.input+split_ext)
        os.remove(args.input+split_exd)
      except OSError as e:
        print(e,file=logfile)
        
    # -------- report total size of the index
    tot_compr = os.path.getsize(index_name)+os.path.getsize(delta_name)
    print("Total size of the index:",tot_compr)
    print("Compression ratio:",tot_compr/os.path.getsize(args.input))

  print("==== Done")
  sys.exit(0)

# check correctness of number of input file and define basename for output
def check_input(args):
  #if args.strict and args.nostrict:
  #  print("Mode can be either strict or non-strict, not both")
  #  sys.exit(1)
  if args.wsize<=2 or args.wsize>128:
    print("Windows size must be in the range [3,128]")
    sys.exit(2)
  if args.bsize<8:
    print("Block size must be at least 8")
    sys.exit(3)
  if args.encoder not in Encoders:
    print("Unknow encoder. Select one from "+Enc_list)
    sys.exit(4)


# execute command: return True is everything OK, False otherwise
def execute_command(command,logfile,logfile_name,env=None):
  try:
    subprocess.run(command.split(),stdout=logfile,stderr=logfile,check=True,env=env)
  except subprocess.CalledProcessError:
    print("Error executing command line:")
    print("\t"+ command)
    print("Check log file: " + logfile_name)
    return False
  return True

  
if __name__ == '__main__':
    main()
 
