#!/usr/bin/env python3
# patextract.py
# Copyright 2015-2016 Giovanni Manzini
import sys, argparse, random, struct


Description = """Tool to randomly extract a set of patterns from a file containing int32 values"""

def main():
  # show_command_line(sys.stderr)
  parser = argparse.ArgumentParser(description=Description)
  parser.add_argument('file', help='input file', type=str)
  parser.add_argument('-p', '--patterns', help='number of patterns to extract (def 1)', default=1, type=int)  
  parser.add_argument('-l', '--length', help='length of each pattern (def 20)', default=20, type=int)  
  args = parser.parse_args()
  assert args.patterns>0, "Invalid number of patterns"  
  assert args.length>0, "Invalid pattern length"  
  # run the algorithm
  with open(args.file,"rb") as f:
    values = []   # read values
    while True:
      z = f.read(4)
      if(len(z)!=4):
        assert len(z)==0, "Invalid input file"
        break
      values.append(struct.unpack('<i',z)[0])
  # file stored in values[]
  print(len(values), "values in file", args.file, file=sys.stderr)
  # do the sampling 
  samples = random.sample(range(0, len(values)+1-args.length),args.patterns)
  # sort and write to stdout 
  samples.sort()
  for s in samples:
    print(*values[s:s+args.length])
  exit(0)

  
if __name__ == '__main__':
    main()
 
