"""
Miscellaneous function to handle bits buffers
(I/O and integer coding) 
"""
import io,struct

# constants defining the type of data to be encoded
Minimal=0; Intermediate=1; Maximal=2; Order=3


# -- class for sending bits to a binary file
class BitBufferOut:
  
  def __init__(self,outfile,encoder):
    assert isinstance(outfile, io.BufferedIOBase)
    self.out = outfile
    self.buffer = 0
    self.buffer_size = 0
    self.bytes_written = 0
    # define function to be used for encoding 
    self.encoder = encoder
    
  def write(self,value,bits):
    assert value < (1<<bits), "Illegal value/bits pair: %d/%d" % (value,bits)
    assert self.buffer_size < 8
    self.buffer |= (value << self.buffer_size)
    self.buffer_size += bits
    assert  self.buffer_size < 72, "Codeword %d bits long" % bits 
    while self.buffer_size>=8:
      self.out.write(struct.pack('B',self.buffer & 255))
      self.buffer >>= 8
      self.buffer_size -= 8
      self.bytes_written += 1

  def encode(self,t,n,data_type):
    assert 0 <= t < n, "Illegal request"
    self.write(*self.encoder(t,n,data_type))

  def flush(self):
    while self.buffer_size>0:
      self.out.write(struct.pack('B',self.buffer & 255))
      self.buffer >>= 8
      self.buffer_size -= 8
      self.bytes_written += 1
    return self.bytes_written


class BitBufferInput:
  def __init__(self,infile,decoder):
    assert isinstance(infile, io.BufferedIOBase)
    self.inf = infile
    self.buffer = 0
    self.buffer_size = 0
    self.decoder = decoder

  def read(self,bits):
    assert 0 < bits < 56, "Illegal request"
    while self.buffer_size<bits:
      z = struct.unpack('B',self.inf.read(1))[0]
      self.buffer |= z<< self.buffer_size
      self.buffer_size += 8
    # extract the requested number of bits  
    v= self.buffer & ((1<<bits)-1) # get lower order bits 
    self.buffer >>= bits
    self.buffer_size -= bits 
    assert self.buffer_size<8 
    return v

  def decode(self,n,_type):
    return self.decoder(self,n,_type)  #always type 0

  # possibly called when a logical block ends. There should be
  # no more valid data   
  def logical_flush(self):
    assert self.buffer_size<8
    assert self.buffer == 0

 



#--------------------------------------------------------------------
# functions for log-skewed encoding/decoding
# See A simple and fast DNA compressor. Softw., Pract. Exper. 34(14): 1397-1411 (2004)
#--------------------------------------------------------------------

# log-skewed encoding returning a 0/1 string
# encodes a value t assuming encoder and decoder know 0 <= t < n
# useful only for debugging
def ls_encode(t,n):
  assert 0 <= t < n, "Illegal input parameters"
  if n==1:
    return ""
  log2 = 1
  pot2 = 2
  while pot2 < n:
    pot2 *=2
    log2 +=1
  if pot2==n:
    return bin(t)[2:].zfill(log2)
  else:
    d = n - pot2//2
    if t>=d:
      return (bin(t-d)[2:].zfill(log2-1)) + "1"
    else:
      return ls_encode(t,d) + "0"

# typed version, for compability with the multi-type encoders
def lsz_encode_typed(t,n,_type):
  return lsz_encode(t,n)

# as ls_encode but returns an integer containing the binary encoding
# and the number of bits   
def lsz_encode(t,n):
  assert 0 <= t < n, "Illegal input parameters: %d %d" % (t,n)
  if n==1:
    return 0,0
  log2 = 1; pot2 = 2
  while pot2 < n:
    pot2 *=2; log2 +=1
  if pot2==n:
    return t,log2
  else:
    d = n - pot2//2
    if t>=d:
      return 2*(t-d) + 1, log2   # code + "1" 
    else:
      v,bits = lsz_encode(t,d)
      return 2*v,bits+1           # code + '0' 

# version in which the code is written directly to the bitbuffer
# works but not currently used 
def lsbb_encode(bitbuffer,t,n):
  assert 0 <= t < n, "Illegal input parameters: %d %d" % (t,n)
  if n==1:
    return  #output nothing 
  log2 = 1; pot2 = 2
  while pot2 < n:
    pot2 *=2; log2 +=1
  if pot2==n:
    bitbuffer.write(t,log2)
  else:
    d = n - pot2//2
    if t>=d:
      bitbuffer.write(1,1) # "1" + code 
      bitbuffer.write(t-d,log2-1)
    else:
      bitbuffer.write(0,1) # "0" + recursive code 
      lsbb_encode(bitbuffer,t,d)
  return   


# decode a string ls_encoded
# get enough bits from the bitstring s
# to decode a value t such that 0 <= t < n 
def ls_decode(s,n):
  if n==1:
    return (0,0)
  log2 = 1; pot2 = 2
  while pot2 < n:
    pot2 *=2; log2 +=1
  if pot2==n:
    return int(s[-log2:],2), log2
  else:
    d = n - pot2//2
    if s[-1]=='1':
      return  d+ int(s[-log2:-1],2), log2
    else: 
      v,bits = ls_decode(s[:-1],d)
      return v, bits+1


# as above but works with integers
# input integer z, bits in z (least significant) containing data, n
# output: decoded value and number of bits consumed 
def lsz_decode(z,bits,n):
  if n==1:
    return (0,0)
  log2 = 1; pot2 = 2
  while pot2 < n:
    pot2 *=2; log2 +=1
  mask = pot2-1  
  if pot2==n:
    assert bits >= log2, "not enough bits: %d vs %d" % (bits, pot2)
    return z & mask, log2
  else:
    d = n - pot2//2
    if z&1!=0:
      # LSB==1
      assert bits >= log2, "not enough bits: %d vs %d" % (bits, pot2) 
      return  d + ((z & mask)>>1), log2
    else: 
      # LSB ==0
      v,b = lsz_decode(z>>1,bits-1,d)
      return v, b+1


# decode an integer lsz_encoded
# get enough bits from the BitBuffer self
# to decode a value t such that 0 <= t < n
# return the decodes value 
def lsbb_decode(bitbuffer,n):
  if n==1:
    return 0   # nothing to read
  log2 = 1; pot2 = 2
  while pot2 < n:
    pot2 *=2; log2 +=1
  if pot2==n:
    return bitbuffer.read(log2)
  else:
    d = n - pot2//2
    bit = bitbuffer.read(1)
    if bit!=0:
      # LSB==1
      assert log2>1, "invalid decoding: %d" % n
      return   d + bitbuffer.read(log2-1)
    else: 
      # LSB ==0
      return lsbb_decode(bitbuffer,d)

# typed version, for compability with the multi-type encoders
def lsbb_decode_typed(bitbuffer,n,_type):
  return lsbb_decode(bitbuffer,n)

# test log skewing encoding/decoding
def ls_test(n):
  for i in range(0,n):
      x = ls_encode(i,n)
      xn,bits = lsz_encode(i,n)
      assert bits==len(x), "lsz length error at %d" % i
      if bits>0:
        assert xn== int(x,2), "lsz value error at %d" % i
      v,y = ls_decode("1010101"+x,n)
      assert v==i, "wrong value: %d vs %d (%s)" % (v,i,x)
      assert y==len(x), "wrong length"
      vn,yn = lsz_decode( (13 <<bits)+ xn,bits+5,n)
      assert vn==i, "wrong lsz value: %d vs %d (%d)" % (i, vn, n)
      assert yn==bits, "wrong lsz lenght"
  return True

# ------------ classical delta encoding -----------
def delta_encode(t,n):     # note: n is  not used
  t+=1  # delta code works for positive values
  log2 = 1; pot2 = 2
  while pot2 <= t:
    pot2 *=2; log2 +=1
  # t has log2 digits
  t0 = t & (pot2//2 -1) # kill MSB
  v = pot2//2 | (t0<<log2)  # unary encoding of log2-1 followed by digits of t 
  return v, 2*log2 - 1

# typed version, for compability with the multi-type encoders
def delta_encode_typed(t,n,_type):  # note: n/data_type are not used
  return delta_encode(t,n)
    
def deltaz_decode(z):
  assert z!=0, "Illegal input"
  b = 0; pot2=1
  while (z & 1) == 0:
    b += 1
    pot2 *= 2
    z = (z >> 1)
  z = (z >> 1)  # discard one bit 
  # b is the number of bits to be extracted
  v = z & (pot2-1) # mask for b ones, works also for b=0 & pot2==1 
  return (v|pot2)-1, 2*b+1 # -1 is because delta is for values>0
  
def deltabb_decode(bitbuffer,n):
  # note: n is not used
  b = 0; pot2=1
  while bitbuffer.read(1) == 0:
    b += 1
    pot2 *= 2
  # b is the number of bits to be extracted
  v = 0 if b==0 else bitbuffer.read(b)
  return (v|pot2)-1 # -1 is because delta is for values>0  
  
# typed version, for compability with the multi-type encoders
def deltabb_decode_typed(bitbuffer,n,_type):
  return deltabb_decode(bitbuffer,n) 
 
def delta_test(n):
  for i  in range(0,n):
      xn,bits = delta_encode(i,n)  
      vn,yn = deltaz_decode( (13 <<bits)+ xn)
      assert vn==i, "wrong deltaz value: %d vs %d (%d)" % (i, vn, n)
      assert yn==bits, "wrong deltaz lenght"
  return True


# ------------------- logskewed +delta -----------------------------
# encoding consisting of a mixture of delta and logskewed encoding
# Given 0 <= t < n we first add 1 to it to get 0 < t <= n
# Let log2t the number of bits in t and log2n number of bits in n
# we use log skewed to encode log2t-1 < log2n
# then we encode the log2t-1 bits of t eclufding the MSB one which is always 1 
# return (v,k) where the ecoding in the lsb bits of v and the total number
# of bits is in k (data_type is ignored)
def lsdz_encode(t,n):
  t +=1  # the encoded value must be >0
  assert  t<= n, "Invalid input"
  # compute # of bits in n
  log2n = 1; pot2n = 2
  while pot2n <= n:
    pot2n *=2; log2n +=1
  # compute # of bits in t
  log2 = 1; pot2 = 2
  while pot2 <= t:
    pot2 *=2; log2 +=1
  # log-skewed encode log2-1 wrt log2n  
  vlog,loglog = lsz_encode(log2 -1, log2n)
  t0 = t & (pot2//2 -1) # kill MSB of t (log2-1 bits remain)
  v = vlog | (t0<<loglog)  # unary encoding of log2-1 followed by digits of t 
  return v, loglog + log2 - 1


# typed version, for compability with the multi-type encoders
def lsdz_encode_typed(t,n,_type):
  return lsdz_encode(t,n)
    
# inverse of the previous function
# z contains bits bits that can be used to decode
# a value t, 0<= t < n 
def lsdz_decode(z,bits,n):
  # compute # of bits in n
  log2n = 1; pot2n = 2
  while pot2n <= n:
    pot2n *=2; log2n +=1
  log2t, loglog = lsz_decode(z,bits,log2n)
  # update buffer 
  assert loglog <= bits
  z = z>>loglog
  # get log2t bits from z 
  pot2t = 1 << log2t
  v = (z & (pot2t-1))| pot2t
  return v-1, loglog + log2t

# as above, but the input is taken from a bitbuffer   
def lsdbb_decode(bitbuffer,n):
  # compute # of bits in n
  log2n = 1; pot2n = 2
  while pot2n <= n:
    pot2n *=2; log2n +=1
  log2t = lsbb_decode(bitbuffer,log2n)
  # get log2t bits from z 
  v = 0 if log2t==0 else bitbuffer.read(log2t)
  #pot2t = 1 << log2t  
  #v = (v & (pot2t-1))| pot2t   masking with pot2t-1 not necessary
  v = v | (1 << log2t)
  return v-1

# typed version, for compability with the multi-type encoders
def lsdbb_decode_typed(bitbuffer,n,_type):
  return lsdbb_decode(bitbuffer,n)
  
  
# test log skewing delta encoding/decoding
def lsd_test(n):
  for i in range(0,n):
      xn,bits = lsdz_encode(i,n)
      vn,yn = lsdz_decode( (13 <<bits)+ xn,bits+5,n)
      assert vn==i, "wrong lsdz value: %d vs %d (%d)" % (i, vn, n)
      assert yn==bits, "wrong lsdz lenght: %d vs %d (%d %d)" % (bits, yn, i,  n)
  return True
  
  
#  -- mixed encoding: the encoder changes according to the type of data
# log skewed + log_skewed_delta
def mix_encode(t,n,_type):
  if _type==Maximal or _type==Minimal:
    return lsdz_encode(t,n)  # for minimal and maximal usa log-skewed delta
  elif _type==Order or _type==Intermediate: 
    return lsz_encode(t,n)
  assert False, "Unknown data type: %d" % _type
  
def mix_decode(bitbuffer,n,_type):
  if _type==Maximal or _type==Minimal:
    return lsdbb_decode(bitbuffer,n)  # for minimal and maximal usa log-skewed delta
  elif _type==Order or _type==Intermediate: 
    return lsbb_decode(bitbuffer,n)
  assert False, "Unknown data type: %d" % _type


#  -- mixed encoding: the encoder changes according to the type of data
# log skewed + delta
def mid_encode(t,n,_type):
  if _type==Maximal or _type==Minimal:
    return delta_encode(t,n)  # for minimal and maximal usa log-skewed delta
  elif _type==Order or _type==Intermediate: 
    return lsz_encode(t,n)
  assert False, "Unknown data type: %d" % _type
  
def mid_decode(bitbuffer,n,_type):
  if _type==Maximal or _type==Minimal:
    return deltabb_decode(bitbuffer,n)  # for minimal and maximal use delta
  elif _type==Order or _type==Intermediate: 
    return lsbb_decode(bitbuffer,n)
  assert False, "Unknown data type: %d" % _type




