#!/usr/bin/env python3
# xosplit.py
# Copyright 2015-206 Giovanni Manzini
import sys, struct, argparse, time


Description = """Tool to split a stock market file into the ordering/delta components

The stock market file must be in signed 32 bit little endian format
The ordering components are values in the range [1,window_size]
The delta components are unsigned 32 bit integers

The original file can be recovered using the xomerge.py tool"""


def split(infile, wsize):
  assert(wsize>2 and wsize<=255)
  out1name = "%s.%d.xo" % (infile,wsize)
  out2name = "%s.%d.xd" % (infile,wsize)
  out1 = open(out1name,"wb")
  out2 = open(out2name,"wb")
  buffer = []    # init buffer
  with open(infile,"rb") as f:
    while True:
      # get 4 bytes from input file
      s = f.read(4)
      if len(s)==0:
        break
      elif len(s)!=4:
        raise Exception("Illegal input file (not 32bit ints)")
      v = struct.unpack('<i', s)[0]   # decode a 4-byte int
      # handle the first value: output  wsize and actual value
      if len(buffer)==0:
        out1.write(struct.pack('B',wsize)) # this value is ignored
        out2.write(struct.pack('<i',v))
        buffer.append(v)
        continue
      # for all entries except the first one  
      # code for v is the position of the predecessor in the list
      # we scan the list right to left (new to old) 
      code = wsize     # means predecessor not yet found  
      assert 0 < len(buffer)<wsize, "Illegal buffer"
      for i in range(1,len(buffer)+1):
        if buffer[-i]== v:
          code = i
          break      # the closest equal value is the predecessor  
        if buffer[-i] < v and (code==wsize or buffer[-i]>buffer[-code]):
          code = i
      assert 0 < code<=wsize, "Illegal code computation"
      assert code <= 255, "Illegal code value (not a uint8)"  
      out1.write(struct.pack('B',code)) # write code as an unsigned uint8
      if code==wsize:     # code wsize means v is smaller than everyone else 
        delta = min(buffer) - v   # buffer is not empty 
      else:
        delta = v-buffer[-code]       # buffer[code] is the predecessor of v
      assert(delta>=0)     
      assert(delta<pow(2,32))  
      out2.write(struct.pack('<I',delta)) # write delta as an unsigned uint32       
      # update buffer inserting new value as last element      
      buffer.append(v)
      if len(buffer)>=wsize:
        assert len(buffer)==wsize
        buffer.pop(0) # remove first (==oldest) element
  out2.close()
  out1.close()
  


def main():
  # show_command_line(sys.stderr)
  parser = argparse.ArgumentParser(description=Description, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('infile', help='input file name (32bit little endian format)', type=str)
  parser.add_argument('-w', help='window size for the ordering component (default 5)', default=5, type=int)   
  args = parser.parse_args()  
  # run the algorithm
  s1 = time.time()
  split(args.infile,args.w)
  e1 = time.time()
  sys.stderr.write("Elapsed time: %.3f\n" % (e1-s1))

  
if __name__ == '__main__':
    main()
 
