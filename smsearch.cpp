/* smsearch.cpp
 * search/decompress algorithm for the stock market index
 * Copyright 2015-2016 Giovanni Manzini
 * */ 
#include <sdsl/suffix_arrays.hpp>
#include <fstream>
#include <string>
#include <ctime>
#include "smindex.hpp"

using namespace std;
#define Mandatory_args 3


// -----------------------------------------------
// Exception to report an error message and stop 
struct SmSearchError: public std::runtime_error
{
    SmSearchError(std::string const& message)
        : std::runtime_error(message)
    {}
};

void read_patterns(string& pname, vector<vector<int32_t>>& pats, int wsize)
{
  ifstream f(pname,ios::in); 
  if(!f.is_open())
      throw SmSearchError("Unable to open pattern file " + pname);
  string line;
  while(getline(f,line)) {
    vector<int32_t> p;
    line2vector(line,p);
    if(p.size() <= (unsigned) wsize) {
      cerr << "Invalid pattern. Patterns must be longer than the window size (";
      cerr << wsize << ")\n";
      exit(EXIT_FAILURE);
    }
    pats.push_back(p);
  }
  f.close();
}

void usage(char *name)
{
    cerr << "Usage:\n    " << name << " base wsize enc [pattern_file] [outfile] [-hsv]" << endl;
    cerr << "    Order preserving search of patterns from PATTERN_FILE" << endl;
    cerr << "    in index for file BASE using a window of size WSIZE and encoder ENC\n";
    cerr << "    If OUTFILE is provided the positions of the OP matchings are written there\n";
    cerr << "    If PATTERN_FILE and OUTFILE are not provided the index is decompressed\n";
    cerr << "    to a file with extension dzi (or ezi for a strict index)\n";
    cerr << "    Options:\n";
    cerr << "      -h   This help message\n";
    cerr << "      -s   The index is strict (handles equal values)\n";
    cerr << "      -v   Verbose output to stderr (decompression only)\n";
    exit(1);
}


int main(int argc, char** argv) 
{
  int c;
  bool strict = false;
  bool verbose = false;
  // ----------- parse options 
  while ((c = getopt (argc, argv, "svh")) != -1)
    switch (c) {
      case 's':
        strict = true;
        break;
      case 'v':
        verbose = true;
        break;
      case 'h':  
      default:
        usage(argv[0]);
    }
  // --- check mandatory args are there 
  argc -= (optind -1);  // get rid of optional arguments  
  if(argc+1 < Mandatory_args) 
    usage(argv[0]);
  argv += optind-1;
  // --------- parse mandatory args 
  string base(argv[1]);
  int wsize = atoi(argv[2]);
  string enc(argv[3]);
  SmIndex smIndex(base,wsize,enc,strict); // create (strict) index
  if(argc==Mandatory_args+1) { // decompress index
    string outfile = base+"."+enc+"."+to_string(wsize);
    outfile += "."+to_string(TEXT_DENS)+(strict ? ".ezi" :".dzi");
    smIndex.decompress(outfile,verbose);
  }
  else { // search for patterns
    ofstream f;
    bool const measure_time = (argc<=Mandatory_args+2);
    if(!measure_time) 
      f.open(argv[Mandatory_args+2]);
    // read patterns from file
    string pname(argv[Mandatory_args+1]);
    vector<vector<int32_t>> pats;
    read_patterns(pname,pats,wsize);
    // main loop
    int64_t total = 0, step1=0, step2=0;
    vector<int64_t> pos;
    pair<int64_t,int64_t>  falsepositives;
    clock_t t=clock();
    for(unsigned i=0;i<pats.size();i++) {
      pos.clear();
      falsepositives = smIndex.locate(pats[i],pos);
      total += pos.size();
      step1 += falsepositives.first;
      step2 += falsepositives.second;
      if(!measure_time) {
        sort(pos.begin(),pos.end());
        for(unsigned i=0;i<pos.size();i++)
          f << pos[i] << " ";
        f << endl;
      }
    }
    t = clock() - t;
    if(!measure_time)
      f.close();
    cout << " Found " << total << " Ph2 " << step2;
    cout << " Ph1 " << step1;
    cout << " Elapsed " << ((double) t)/CLOCKS_PER_SEC << " seconds\n";
  }
  return 0;
}
