#include "mycsa.hpp"  // definition of my_csa type
#include "delta.hpp"  // DeltaFile class and subclasses
#include "opmatch.hpp"

using namespace std;



// -----------------------------------------------
// Exception to report an error message and stop 
struct SmIndexError: public std::runtime_error
{
    SmIndexError(std::string const& message)
        : std::runtime_error(message)
    {}
};


class SmIndex 
{
  protected:
  string base_name;    // base name of the input file
  my_csa index;        // fm_index built on the order values
  int64_t size;        // size of the index (bwt) == text_size+1
  bool strict;         // whether this is a strict index 
  DeltaFile delta;
    
  public:
  int64_t text_size;             // elements in original file
  int wsize;                     // window size 
  const int bsize = TEXT_DENS;   // block_size

  SmIndex(string _name, int _w, string _enc, bool _strict) : base_name(_name), delta(_name,_w,_enc,_strict) 
  {
    // record if this is a strict index 
    strict = _strict;
    // init fm-index of order component
    string ext = "."+to_string(_w)+(strict?".yo.":".xo.")+to_string(TEXT_DENS)+".csa";
    if(!load_from_file(index, _name+ext))
      throw SmIndexError("Unable to load index " + _name+ext);
    // init sizes  
    size =   index.size();
    text_size = index.size()-1;
    // init wsize both here and inside DeltaFile 
    wsize = _w;
    delta.wsize = _w;
    // consistency check
    assert(delta.nblocks==(text_size+bsize-1)/bsize);
  }

  // to_s like function 
  std::ostream& dump(std::ostream& ris);
  // search the OP-occcurencce of p storing them in pos
  pair<int64_t,int64_t> locate(vector<int32_t>& p, vector<int64_t>& pos);
  // decompress the sm-values and store them to outfile
  void decompress(string &outfile, bool verbose);
  
  protected:
  // extract order values from the csa
  int64_t extract_order(uint8_t *b);  
  // extract the prefix of a block fiven block number and order information
  void extract_block(int64_t bnum, int k, uint8_t *obuf, int32_t *block);
  // compute order vector 
  void compute_order(vector<int32_t>& p, uint8_t *o);
  // compute order vector handling equal values  
  void compute_order_strict(vector<int32_t>& p, uint8_t *o);
  // single step of backward search
  int64_t backward_step(int64_t l, int64_t r, int c, int64_t& lres, int64_t& r_res);
  // backward search of string s
  int64_t backward_search(uint8_t *, int, int64_t& lres, int64_t& r_res);
  // locate a single row of the bwt-matrix
  int64_t csa_locate(int64_t p);
  int64_t csa_locate(int64_t p, uint8_t *b, int64_t &steps);
  // recursive op-locate function 
  int64_t explore_rec(uint8_t *const porder, uint8_t *const torder, vector<int32_t> &, int i, 
             int64_t b, int64_t e,  vector<int64_t> &pos);
  // recursive op-locate function for strict search 
  int64_t explore_rec_strict(uint8_t *const porder, uint8_t *const torder, 
             vector<int32_t> &, vector<int32_t> &, int i, 
             int64_t b, int64_t e,  vector<int64_t> &pos);
  // extract a subsequence of sm-values            
  void extract(int64_t start, int k, uint8_t *obuf, int32_t *vbuf);
};



// locate all the text positions which OP-match the pattern p[]
// the positions are stored into pos[]
// return the pair consisting of the number of step1/step2 false positives
pair<int64_t,int64_t> SmIndex::locate(vector<int32_t> &p, vector<int64_t> &pos)
{
  // compute order component of pattern
  uint8_t porder[p.size()];
  if(strict)
    compute_order_strict(p,porder);
  else
    compute_order(p,porder);

  // step1: fast csa-search for the last p.size()-wsize +1 entries
  int64_t begin=0,end= -1;
  int64_t step2, step1 = backward_search(porder + (wsize-1), p.size() -wsize + 1, begin,end);
  if(step1==0) return make_pair(0,0);
  
  
  // step2: some csa rows need to be verified  
  // store the ordering of p[] into psort<>[]
  vector<int32_t> psort;
  pattern_preprocess(p,psort); 
  // allocate buffer to store order values for subsequences of t[]
  uint8_t torder[p.size()+bsize];
  for(unsigned i=0;i<p.size();i++) {        // copy tail of porder values 
    torder[i+bsize] = porder[i];            // to the tail of the torder buffer
    // cerr << (int) porder[i]<<" ";//!!!!!!!!
  }
  // cerr << "---\n"; //!!!!!! 
  // extend each tail backward and check if they are real op-matches
  if(strict)
    step2 = explore_rec_strict(porder, torder, p, psort, wsize-2, begin, end, pos);
  else  
    step2 = explore_rec(porder, torder, psort, wsize-2, begin, end, pos);

  return make_pair(step1,step2);
}


/* Second phase of the non-strict search algorithm 
 * if m  denotes the length of the pattern:
 *  porder[m] order component of pattern (porder[0] is useless)
 *  torder[m+bsize] buffer for the order component of text; the last m -wsize+1 are filled
 *  psort[m] sorted order of pattern values
 *  i pattern positions to be considered (initially wsize-2, recall porder[0] is useless) 
 *  b,e range in the csa correponding to phase1 search (last m-wsize+1 elements of porder) 
 *  pos[] array where actual matches are stored 
 * */
int64_t SmIndex::explore_rec(uint8_t *const porder, uint8_t *const torder, 
             vector<int32_t>& psort, int i, 
             int64_t b, int64_t e,  vector<int64_t> &pos)
{
  int64_t num, newb, newe,tot_check=0;
  
  if(i>0) {
    assert(porder[i]==wsize or porder[i]<=i);
    num = backward_step(b,e,porder[i],newb, newe);
    if(num>0) {
      torder[bsize+i] = porder[i];
      tot_check += explore_rec(porder, torder, psort, i-1, newb, newe, pos);
    }
    for(int j=i+1;j<wsize;j++) {
      assert(porder[i]!=j);
      num = backward_step(b,e,j,newb, newe);
      if(num>0) {
        torder[bsize+i] = j;
        tot_check += explore_rec(porder, torder, psort, i-1, newb, newe, pos);
      }
    }
  }
  else {   // i==0 recursion stops
    for(int64_t row=b; row <= e; row++) {
      // one step backward to read the value corresponding to p[0]
      if((torder[bsize]=index.bwt[row])==0) continue;
      // locate the entry corresponding to t[0]
      int64_t steps;
      int64_t tpos = csa_locate(index.lf[row], torder + bsize, steps);
      assert(steps<bsize);
      // decompress the portion from tpos-steps till tpos+psize
      int32_t block[steps+psort.size()];
      extract(tpos-steps,steps+psort.size(),torder +(bsize-steps), block);
      tot_check += 1;
      if(op_match_check(block+steps,psort))
        pos.push_back(tpos);
    }
  }
  return tot_check;
}


/* Second phase of the non-strict search algorithm 
 * let m denote the length of the pattern:
 *  porder[m] order component of pattern (porder[0] is useless)
 *  torder[m+bsize] buffer for the order component of text; the last m -wsize+1 are filled
 *  p[m] the pattern to be searched
 *  psort[m] sorted order of pattern values
 *  i pattern positions to be considered (initially wsize-2, recall porder[0] is useless) 
 *  b,e range in the csa correponding to phase1 search (last m-wsize+1 elements of porder) 
 *  pos[] array where actual matches are stored 
 * */
int64_t SmIndex::explore_rec_strict(uint8_t *const porder,  uint8_t *const torder, 
             vector<int32_t> &p, vector<int32_t>& psort, int i, 
             int64_t b, int64_t e,  vector<int64_t> &pos)
{
  int64_t num, newb, newe,tot_check=0;
  
  if(i>0) {
    assert(porder[i]==1 or porder[i]/2<=i); // order must point inside the pattern
    num = backward_step(b,e,porder[i],newb, newe); // search same order
    if(num>0) {
      torder[bsize+i] = porder[i];
      tot_check += explore_rec_strict(porder, torder, p, psort, i-1, newb, newe, pos);
    }
    if(porder[i]%2!=0) { // only if p[i] is not equal to a previous value, go outside p[]
      for(int j=2*(i+1);j<2*wsize;j++) { // predecessor, possibly equal outside p[]
        assert(porder[i]!=j);
        num = backward_step(b,e,j,newb, newe);
        if(num>0) {
          torder[bsize+i] = j;
          tot_check += explore_rec_strict(porder, torder, p, psort, i-1, newb, newe, pos);
        }
      }
    }
  }
  else {   // i==0 recursion stops
    for(int64_t row=b; row <= e; row++) {
      // one step backward to read the value corresponding to p[0]
      if((torder[bsize]=index.bwt[row])==0) continue;
      // locate the entry corresponding to t[0]
      int64_t steps;
      int64_t tpos = csa_locate(index.lf[row], torder + bsize, steps);
      assert(steps<bsize);
      // decompress the portion from tpos-steps till tpos+psize
      int32_t block[steps+psort.size()];
      extract(tpos-steps,steps+psort.size(),torder +(bsize-steps), block);
      tot_check += 1;
      if(op_strict_match_check(block+steps,p,psort))
        pos.push_back(tpos);
    }
  }
  return tot_check;
}

               

// extract (decompress) k sm-values starting from position start
// the corresponding order values must be already stored in obuf[0:k]
// the extracted values are stored in vbuf[0:k] which must have been
// properly allocated  
void SmIndex::extract(int64_t start, int k, uint8_t *obuf, int32_t *vbuf)
{
  assert(k>0);
  assert(start%bsize==0);
  int64_t bnum = start/bsize;
  while(k>0) {
    if(k<bsize) {
      extract_block(bnum, k, obuf, vbuf);
      break;
    }
    extract_block(bnum, bsize, obuf, vbuf);
    bnum+= 1;  k -= bsize;
    obuf += bsize; vbuf += bsize;
  }
}

// extract (decompress) a block (up to k elements) given the corresponding order values 
// block should have been properly allocated of size at least k
void SmIndex::extract_block(int64_t bnum, int k, uint8_t *obuf, int32_t *block)
{
  delta.extract_block(bnum,k,obuf,block);
}


// extract the all the text (in this case order values) underlying the index,
// writing them to b[], which should have been properly allocated
// return # characters extracted 
int64_t SmIndex::extract_order(uint8_t *b)
{
  const auto& lf = index.lf;
  int64_t i,j=0;
  for (i=0;index.bwt[j]!=0; ++i){
    b[i] = index.bwt[j];
    j = lf[j];
  }
  // reverse segment  
  for(j=0; j <i-1-j; j++) {
    uint8_t t = b[i-1-j];
    b[i-1-j] = b[j];
    b[j] = t;
  }
  return i;
}


void SmIndex::decompress(string &outfile, bool verbose)
{
  if(verbose)
    cerr << "Starting decompression" << endl;
  // extract order file (slow)
  uint8_t *o = new uint8_t[text_size];
  int64_t i = extract_order(o);
  assert(i==text_size);
  
  // open output file
  ofstream out(outfile,ios::out|ios::binary);
  // extract blocks one at a time 
  int32_t block[TEXT_DENS];
  int64_t num_blocks = (text_size+TEXT_DENS-1)/TEXT_DENS;
  for(i=0;i<text_size;i+= TEXT_DENS) {
    int64_t bnum = i/TEXT_DENS;
    if(verbose and (num_blocks< 10 or bnum%(num_blocks/10)==0)) 
      cerr << "decompressing block " <<  bnum << endl;
    int size = min((int64_t)TEXT_DENS,text_size-bnum*TEXT_DENS);  
    extract_block(bnum, size, o+i, block);
    out.write((char *)block,sizeof(*block)*size);  // warning: this is not endian-safe 
  }
  out.close();
  delete[] o;
}



// write an index description to ostream, used by the overloaded << 
std::ostream& SmIndex::dump(std::ostream& ris) {
    if(strict) ris << "strict "; 
    ris << "SmIndex<"<< TEXT_DENS << "," << TEXTI_DENS << "> of size ";
    ris << size_in_bytes(index) <<  endl;
    ris << "Built on a order file of size " << text_size;
    ris << " computed using a window of size " << wsize << endl;
    ris << "DeltaFile<"<<delta.type + "> of size ";
    ris << delta.size << " contains " << delta.nblocks << " blocks" << endl;
    ris << "Indexed file: "<< base_name;
    return ris;
}

// overloading for operator << 
std::ostream& operator<<(std::ostream& o, SmIndex& b) { 
  return b.dump(o); 
}


// ----- protected methods

// compute order component for the pattern 
void SmIndex::compute_order(vector<int32_t>& p, uint8_t *o)
{
  assert(wsize<=255);
  o[0] = 0;                     // this value is not used
  for(int j=1;j<(int)p.size();j++) { //compute o[j]
    int code = wsize;
    for(int i=1;i<wsize and j-i>=0;i++) {
      if(p[j-i]==p[j]) {
        code = i;
        break; // the closest equal value is the predecessor
      }
      if(p[j-i]<p[j] and (code==wsize or p[j-i]>p[j-code]))
        code = i;
    }
    assert(code<=wsize and code>0);
    o[j]=code;
  }
}

// compute order component for the pattern in strict mode
void SmIndex::compute_order_strict(vector<int32_t>& p, uint8_t *o)
{
  assert(wsize<=128);
  o[0] = 0;                     // this value is not used
  for(int j=1;j<(int)p.size();j++) { //compute o[j]
    int code = 1;
    for(int i=1;i<wsize and j-i>=0;i++) {
      if(p[j-i]==p[j]) {
        code = 2*i;
        break; // the closest equal value is the predecessor and generates an even code
      }
      if(p[j-i]<p[j] and (code==1 or p[j-i]>p[j-code/2]))
        code = 2*i+1;
    }
    assert(code< 2*wsize and code>0);
    o[j]=code;
  }
}



int64_t SmIndex::backward_step(int64_t l, int64_t r, int c, int64_t& l_res, int64_t& r_res)
{
  // taken from suffix_array_algorithm.hpp
    assert(l <= r); assert(r < size);
    assert(c>0); // added by me
    int cc = index.char2comp[c];
    if (cc == 0) {
        l_res = 1;
        r_res = 0;
    } else {
        int64_t c_begin = index.C[cc];
        if (l == 0 and r+1 == size) {
            l_res = c_begin;
            r_res = index.C[cc+1] - 1;
        } else {
            l_res = c_begin + index.bwt.rank(l, c); // count c in bwt[0..l-1]
            r_res = c_begin + index.bwt.rank(r+1, c) - 1; // count c in bwt[0..r]
        }
    }
    assert(r_res+1-l_res >= 0);
    return r_res+1-l_res;
}


int64_t SmIndex::backward_search(uint8_t *s, int len, int64_t& l_res, int64_t& r_res)
{
  int64_t l=0,r=size-1,num=size;  
  for(int i=len-1;i>=0;i--) {
    int c = s[i];
    num = backward_step(l,r,c,l_res,r_res);
    if(num==0) return 0;
    l=l_res; r = r_res;
  }
  return num;
}  


// plain locate operation for a fm-index  
int64_t SmIndex::csa_locate(int64_t p)
{
  int64_t pos=0;
  while(!index.sa_sample.is_sampled(p)) {
    pos++;
    p = index.lf[p];
  }
  return pos+index.sa_sample[p];
}  


// enriched locate operation for a fm-index  
// write backward the extracted character in b, starting from b[-1],  
// and store in steps the number of extracted chars
int64_t SmIndex::csa_locate(int64_t p, uint8_t *b, int64_t &steps)
{
  steps=0;
  while(!index.sa_sample.is_sampled(p)) {
    --b;
    *b = index.bwt[p];
    assert(*b>0);
    steps++;
    p = index.lf[p];
  }
  return steps+index.sa_sample[p];
}  
