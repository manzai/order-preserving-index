# SM-tools: Order Preserving indexing and compression

*SM-tools* (Stock Market tools) is a collection of tools for compressing and searching data in the order-preserving model. 

Given a file containing 32-bit integers SM-tools compress it in a format taking roughly the same space used by **gzip**. The output file is not only a compressed version of the input but, more importantly, a compressed index for the *order-preserving* matching problem. Given a pattern *P* using the compressed index we can find the substring of the original input file whose elements are in the same relative order as the pattern elements. The search is extremely fast since it does not need the complete decompression of the original input.

Formal definitions, the description of the index structure and of the search algorithm are available on the [DCC paper](https://doi.org/10.1109/DCC.2017.35) by Gianni Decaroli, Travis Gagie, and Giovanni Manzini. 


Copyright 2016-2018 by the Authors.



## Installation 

* download and install the [sdsl-lite library](https://github.com/simongog/sdsl-lite)

* download/clone this repository

* make sure the location of the library/include files from *sdsl-lite* are in a known position or use the *LIB_DIR* and *INC_DIR* variables in the Makefile

* execute `make` to compile the compressor and search tools with the default parameters. 

* Some of the wrappers are in Python so you must have installed `python3`


## First run

To build the compressed index for the file *data/10000t* containing ten thousands temperature measurements type

		smbuild.py -s data/10000t

this command generates the files *data/10000t.6.yo.32.csa* and *10000t.lsk.6.32.ez* that together constitute the index.

To test the searching features we must first create a pattern file. Pattern files are in text format with one line per pattern. So for example create a text file called *pats* containing the two lines

	1 2 3 4 5 6 7 8 9
	9 8 7 6 5 4 3 2 1

and search them in the index typing

		smsearch.32.x -s data/10000t 6 lsk pats occs

The above command search the patterns in *pats* in the index files we created previously and stores the locations of the patterns in the file *occs*. The output of the command would be

	 Found 3 Ph2 4 Ph1 97 Elapsed 0.000335 seconds

meaning that it found 3 occurrences overall in 0.335 milliseconds (the other figures are the number of false positives and can be ignored). If we look at the content of *occs* we find

```
890 4559 4560

```

meaning that the first pattern (the increasing sequence) was found 3 times in positions 890, 4559, and 4560. The empty second line means the second pattern (the decreasing sequence) never occurs in the input file. Note that positions refer to the integers stored in *10000t* so the actual byte positions are obtained multiplying that values by four. Indeed, if we look at the input file in positions 3560, 18236, and 18240 we see it contains a sequences of 9 increasing integers (note the latter two sequences overlap):

```
> od -t d4 -A d -j 3560 -N 36 data/10000t 
0003560          65          70          79          88
0003576          90          92          93          94
0003592          99
0003596
> od -t d4 -A d -j 18236 -N 36 data/10000t 
0018236          76          77          83          84
0018252          86          87          88          89
0018268          90
0018272
> od -t d4 -A d -j 18240 -N 36 data/10000t 
0018240          77          83          84          86
0018256          87          88          89          90
0018272          91
```

To recover the original file we still use the *smsearch* tool but without supplying the patter and output files:

		smsearch.32.x -s data/10000t 6 lsk

the above command will generate the file *data/10000t.lsk.6.32.ezi* which will be identical to the original file *data/10000t*.

**Note:** all tools assume integers are represented in little-endian order.

### Strict vs non-strict mode

The above commands all use the option `-s` forcing the use of strict mode in which equal values in the pattern only matches with equal values in the text.  If `-s` is not used the tools work in *non-strict* mode where consecutive equal values are considered increasing. Eg. In non-strict mode the pattern `0 0 0 0 0 0 0 0 0` matches `1 1 1 1 1 1 1 1 1` but also `1 1 1 2 2 2 3 3 3` and `1 2 3 4 5 6 7 8 9`.


### Changing the compression parameters

The main compression parameters are the window size (default 6) and the block size (default 32). These parameters can be changed from the command line (help on each command is obtained typing the command name with option `-h`). However, before using an index with a different block size the appropriate executables must be created. For example, to use an index with block size of 64, first you must give the command `make BSIZE=64`.

## Other tools

We provide a naif search algorithm *bruteforce.x* that finds the order preserving occurrences with a simple pass over all positions in the input file. The input parameter and the output format are exactly the same as *smsearch* (indeed we compare their output with *cmp* to verify the correctness of *smsearch* output in [multi-test mode](#multiple-tests). For example

		bruteforce.x -s data/10000t pats bf_occs
produces a file *bf_occs* identical to the file *occs* produced by the *smsearch.32.x* command above. 

The directory *others* contain the source code of two order preserving matching tools described in the papers

* Tamanna Chhabra, Simone Faro, M. Oguzhan Külekci, Jorma Tarhio,  [*Engineering order-preserving pattern matching with SIMD parallelism*](https://doi.org/10.1002/spe.2433). Softw., Pract. Exper. 47(5): 731-739 (2017)

* 	Tamanna Chhabra, M. Oguzhan Külekci, Jorma Tarhio, 
[*Alternative Algorithms for Order-Preserving Matching*](http://www.stringology.org/event/2015/p05.html). Stringology 2015: 36-46

We thank the authors for providing the code and helping us to integrate their algorithms in our environment. We wrote a wrapper so that the above tools can be used in the same way as out tools.  For example:

```
> others/fmop_main data/10000t pats
Input file contains 10000 values
Pattern file contains 2 patterns
Building the index
3 OP matches found. Elapsed 0.000023 seconds 0.011500 millisecXpat
> others/sso_main data/10000t pats
Input file contains 10000 values
Pattern file contains 2 patterns
3 OP matches found. Elapsed 0.000534 seconds 0.267000 millisecXpat
```

Note that our wrapper does not provide the position of the occurrences in the input file, but only their total number. Please read the above papers for further information on the tools.


## Test files

In the experiments we used the following files.

Name            | # Values    |   Description 
----------------|------------:|:--------------------
Prices | 31,559,990 | daily, hourly, and 5min US stock prices downloaded from [http://stooq.com/db/h/](http://stooq.com/db/h/)
Temp   | 30,505,702 | Minimum and maximum daily temperature from 424 US stations over 100 years downloaded from [http://cdiac.ornl.gov/ftp/us_recordtemps/sta424/](http://cdiac.ornl.gov/ftp/us_recordtemps/sta424/). Temperature values are in the range [-57,121]; the value -127 is used to mark one or more consecutive missing measurement
Ecg    | 20,138,750 | 22 hours and 23 minutes of ECG data downloaded from [http://www.cs.ucr.edu/~eamonn/UCRsuite.html](http://www.cs.ucr.edu/~eamonn/UCRsuite.html). From the original sequence we removed a tail of 1250 occurrences of the value -32768 which were not part of the ECG data.
Rwalk  | 50,000,000 | random walk with integer steps in the range [-20,20] 
Rand   | 50,000,000 | random integers in the range [-20,20]
Ran127   | 50,000,000 | random integers in the range [-127,127]

In some experiments we used files with entries in the range [-127,127]. The values in files Temp, Rand, and Rand127 are alredy in this range. For the other three files we produced alternative versions, named Prices0, Ecg0, and Rwalk0 in which values were transformed with the formula $$x \to (x \bmod 255) - 127$$ so that all values are in the [-127,127] range  and, if neighboring values are not too distant, their relative order is usually unchanged.

The above test files are [available for download](https://drive.google.com/drive/folders/18HTESHUH7VE-aec4V2rHxx-EJWWEzcZM?usp=sharing). All values are stored as 32 bit signed integers in little endian format, hence file sizes are 4 times the number of values.


## Multiple tests 

The *Makefile* contains targets designed to test the correctness an speed of *smsearch* on multiple files and for several possible input parameters. Such tests are invoked by python wrappers that output the results in the form of a Latex table. 

To check compression and decompression, and to compare the compression ratio of SM-tools for the files *Temp* and *Prices* in the directory *data* using window of size 3, 6, and 9 and block size 32 and 64 checking decompression and working in strict mode type:

		compare.py Prices:Temp -d data -w 3:6:9 -b 32:64 -s -c
		
and you will eventually get the following table reporting the compression ratio of the SM index for the various parameters compared to gzip  and xz compression ratio.

```
                          & Prices& Temp  \\\hline\hline
\smi  $q\!=\!3, B\!=\!32$ & 0.35  & 0.31  \\\hline
\smi  $q\!=\!6, B\!=\!32$ & 0.37  & 0.33  \\\hline
\smi  $q\!=\!9, B\!=\!32$ & 0.38  & 0.34  \\\hline
\smi  $q\!=\!3, B\!=\!64$ & 0.29  & 0.25  \\\hline
\smi  $q\!=\!6, B\!=\!64$ & 0.30  & 0.26  \\\hline
\smi  $q\!=\!9, B\!=\!64$ & 0.31  & 0.27  \\\hline
\gzip                     & 0.37  & 0.24  \\\hline
\xz                       & 0.24  & 0.17  \\\hline\hline
```

To compare the positions of the occurrences found by the SM-tools with those of the (bruteforce) scan algorithm on the same files and parameters as above,  using 100 patterns of length 20 extracted from the input files type:

		searchtest.py Prices:Temp -d data -w 3:6:9 -b 32:64 -s -c

This will produce the following table showing the the positions of the occurrences match in every experiment. 

```
                 & Prices& Temp  \\\hline
\smi-\lsk   3 32 & ok    & ok    \\\hline
\smi-\lsk   6 32 & ok    & ok    \\\hline
\smi-\lsk   9 32 & ok    & ok    \\\hline
\smi-\lsk   3 64 & ok    & ok    \\\hline
\smi-\lsk   6 64 & ok    & ok    \\\hline
\smi-\lsk   9 64 & ok    & ok    \\\hline
```

Finally to compare the search speed of different of the SM-tools with those of *bruteforce* and the *fmop* algorithm type:

		searchtest.py Prices0:Temp -d data -w 3:6:9 -b 32:64 -s -fmop

and you will get a table reporting the number of occurrences aand search time for each algorithm 
```
                              & Prices & Temp   \\\hline
\lsk $q\!=\!3, B\!=\!32$  occ &    102 &    100 \\\hline
\lsk $q\!=\!3, B\!=\!32$ time &   0.02 &   0.02 \\\hline
\lsk $q\!=\!6, B\!=\!32$  occ &    102 &    100 \\\hline
\lsk $q\!=\!6, B\!=\!32$ time &   0.04 &   0.03 \\\hline
\lsk $q\!=\!9, B\!=\!32$  occ &    102 &    100 \\\hline
\lsk $q\!=\!9, B\!=\!32$ time &   0.10 &   0.03 \\\hline
\lsk $q\!=\!3, B\!=\!64$  occ &    102 &    100 \\\hline
\lsk $q\!=\!3, B\!=\!64$ time &   0.04 &   0.03 \\\hline
\lsk $q\!=\!6, B\!=\!64$  occ &    102 &    100 \\\hline
\lsk $q\!=\!6, B\!=\!64$ time &   0.04 &   0.09 \\\hline
\lsk $q\!=\!9, B\!=\!64$  occ &    102 &    100 \\\hline
\lsk $q\!=\!9, B\!=\!64$ time &   0.05 &   0.08 \\\hline
\fmo occ                      &    102 &    100 \\\hline
\fmo millisecXpat             &   0.17 &   0.19 \\\hline
\scan occ                     &    102 &    100 \\\hline
\scan millisecXpat            & 225.38 & 196.14 \\\hline
```

The number and the length of the patterns can be changed from the command line. Type `searchtest.py -h` for the available options. Note that since the experiments are controlled by a *make* command, old results are reused by default. 


