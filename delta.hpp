#include <iostream>
#include <fstream>
#include <string>
#include "mybits.hpp"
#include "slidingWindow.hpp"

using namespace std;


// -----------------------------------------------
// Exception to report an error message and stop 
struct DeltaFileError: public std::runtime_error
{
    DeltaFileError(std::string const& message)
        : std::runtime_error(message)
    {}
};



class DeltaFile
{
  public:
  int64_t nblocks;      // number of blocks 
  int64_t size=-1;      // size in bytes of the compressed delta file  
  int wsize=-1;         // windows size
  const int bsize=TEXT_DENS; // block size 
  string type;          // type of encoding 
  protected:
  int64_t *block_start; // starting position of each block in buffer 
  uint8_t *buffer;      // compressed bit stream
  bool strict;          // whether this is for a strict index
  BitDecoder *decoder;   // decoder for the delta values 

  public:
  DeltaFile(string _name, int _w, string _enc, bool _strict) : type(_enc), strict(_strict) {
    // ----- open compressed file 
    string ext = "."+type+"."+to_string(_w)+"."+to_string(TEXT_DENS);
    ext += strict ? ".ez" :".dz";
    ifstream in(_name+ext,ios::in|ios::binary|ios::ate);
    if(!in.is_open())
      throw DeltaFileError("Unable to load file " + _name+ext);
    // get file size in bytes (it was opened at the end)
    size =  in.tellg();
    // get number of blocks
    in.seekg (0, in.beg);  // rewind  
    nblocks = read_int(in,4);
    assert(nblocks>0);
    // read block lengths and store block starting points
    block_start = new int64_t[nblocks];
    int64_t next_start=0;
    for(int64_t i=0;i<nblocks;i++) {
      block_start[i] = next_start;
      next_start += read_int(in,2);
    }
    // now start contains the size of the bit buffer (in bytes)
    assert(next_start==size-4-2*nblocks);
    // create and save bit buffer
    buffer = new uint8_t[next_start];
    in.read((char *)buffer,next_start);
    assert(in.gcount()==next_start);
    in.close();
    // ---- init decoder according to type
    if(_enc=="lsd")
      decoder = new BitDecoderlsd();
    else if(_enc=="lsk")
      decoder = new BitDecoderlsk();
    else if(_enc=="dlt")
      decoder = new BitDecoderdlt();
    else if(_enc=="mix")
      decoder = new BitDecodermix();
    else if(_enc=="mid")
      decoder = new BitDecodermid();
    else
      throw DeltaFileError("Unknown encoder: " + _enc);
  }
    
  // destructor deallocate buffers
  ~DeltaFile() {
    // cerr<< "INFO: DeltaFile destructor of type " << type << endl;
    delete decoder;
    delete[] block_start;
    delete[] buffer;
  }
  
  // extract the first k int's from block bnum storing then in block[] 
  void extract_block(int64_t bnum, int k, uint8_t *obuf, int32_t *block);

  private:  
  int64_t read_int(ifstream &f,int bytes);    
  uint32_t get_uint32(uint8_t *b);
  int64_t decode_single(int i, int o, SlidingWindow& window,const uint32_t,const uint32_t);
  int64_t decode_single_strict(int i, int o, SlidingWindow& window,const uint32_t,const uint32_t);

};



// extract the first k int's from block bnum storing then in block[] 
void DeltaFile::extract_block(int64_t bnum, int k, uint8_t *obuf, int32_t *block)
{
  assert(bnum<nblocks);
  assert(k <= bsize);
  assert(k>0);
  uint32_t max_posdelta, max_negdelta;
  int64_t v;
  SlidingWindow window(wsize);
  
  // get starting position of the bit stream
  uint8_t *b = buffer+block_start[bnum];
  // read block header
  block[0] = (int32_t) get_uint32(b);  b+= 4; // first block element
  window.push(block[0]);                      // init sliding window 
  max_posdelta = get_uint32(b);      b += 4;
  max_negdelta = get_uint32(b);      b += 4;
  // init bit decoder for delta values
  decoder->init(b);
  int i = 1;  // # of block values decoded 
  obuf +=1;   // the first order value of each block is discarded 
  while(i<k) {
    int o = *obuf++;       // get order
    if(strict)
      v = decode_single_strict(i,o,window,max_posdelta, max_negdelta);
    else
      v = decode_single(i,o,window,max_posdelta, max_negdelta);
    // --- v is the next value
    block[i++] = (int32_t) v;
    if(i>=wsize) window.pop(); // if (i-1) >= wsize remove oldest
    window.push(v);
  }
}


// decode a single delta value given its position, its order value, 
// the sliding window, and max_[pos|neg]delta constants
// this is the version for a strict index () 
int64_t DeltaFile::decode_single_strict(int i, int o, SlidingWindow& window,
                                 const uint32_t max_posdelta, 
                                 const uint32_t max_negdelta)
{
  int32_t pred, succ;
  int64_t v, delta;

  assert(o < 2*wsize);
  if(o/2>i) { // this is a missing order entry
    o = decoder->decode(i+1,Order); // type Order: missing order entry
    assert(o <= i);
    o = 2*o+1;
  }
  // --- now we have the order value relative to the current buffer
  if(o%2==0)
    v = window.extract(o/2);  // nothing to read from the decoder
  else if (o==1) {  // this is a type 0 minimal entry  
    delta = 1 + decoder->decode(max_negdelta,Minimal);
    assert(delta>0 and delta <= max_negdelta);
    v = window.min() - delta;
  } 
  else {         // type 1 or 2
    pred = window.extract(o/2); // predecessor of the new value
    succ = window.succ(pred);   // successor of the predecessor
    if(pred==succ) {            // type 2 maximal
      delta = 1 + decoder->decode(max_posdelta,Maximal);
      assert(delta>0 and delta <= max_posdelta);
    }
    else {                   // type 1 intermediate
      delta = 1 + decoder->decode(succ-pred-1,Intermediate);
      assert(delta> 0 and delta < succ - pred);
    }
    v = pred + delta;
  }

  return v;
}


// decode a single delta value given its position, its order value, 
// the sliding window, and max_[pos|neg]delta constants 
int64_t DeltaFile::decode_single(int i, int o, SlidingWindow& window,
                                 const uint32_t max_posdelta, 
                                 const uint32_t max_negdelta)
{
  int32_t pred, succ;
  int64_t v, delta;

  assert(o <= wsize);
  if(o!=wsize and o>i) { // this is a missing order entry
    o = decoder->decode(i+1,Order); // type Order: missing order entry
    assert(o <= i);
    if(o==0) o = wsize;
  }
  // --- now we have the order value relative to the current buffer
  if (o==wsize) {  // this is a type 0 minimal entry  
    delta = 1+decoder->decode(max_negdelta,Minimal);
    assert(delta>=0 and delta <= max_negdelta);
    v = window.min() - delta;
  } else {         // type 1 or 2
    pred = window.extract(o); // predecessor of the new value
    succ = window.succ(pred); // successor of the predecessor
    if(pred==succ) {          // type 2 maximal
      delta = decoder->decode(max_posdelta+1,Maximal);
      assert(delta>=0 and delta <= max_posdelta);
    }
    else {                   // type 1 intermediate
      delta = decoder->decode(succ-pred,Intermediate);
      assert(delta>=0 and delta < succ - pred);
    }
    v = pred + delta;
  }
  return v;
}



// read BYTES<=8 bytes from file and store them in a int64_t 
int64_t DeltaFile::read_int(ifstream& f,int bytes)  
{
  assert(bytes <= 8);
  uint8_t buffer[8];
  int64_t r=0;

  f.read((char *) buffer,bytes);
  assert(f.gcount()==bytes);
  for(int i=0;i<bytes;i++) {
    int64_t t = buffer[i]<<(i*8);
    r |= t;
  }
  return r;
}
  
// extract an uint32_t (little endian) from bufer 
uint32_t DeltaFile::get_uint32(uint8_t *b)
{
  uint32_t v=0;
  for(int i=0;i<4;i++) {
    v |= (*b <<(i*8)); b += 1;
  }
  return v;
}


