#include <vector>
#include <algorithm>


using namespace std;


// check order preserving matching 
bool op_match_check(int32_t *text, vector<int32_t> &sorted)
{
  for(unsigned i=0;i<sorted.size()-1;i++) {
    if(text[sorted[i]]< text[sorted[i+1]])
      continue;
    else if(text[sorted[i]]==text[sorted[i+1]] and sorted[i] < sorted[i+1])
      continue;
    return false;
  }
  return true;
}


// check strict order preserving matching 
bool op_strict_match_check(int32_t *text, vector<int32_t> &p, vector<int32_t> &sorted)
{
  for(unsigned i=0;i<sorted.size()-1;i++) {
    if(p[sorted[i]]< p[sorted[i+1]]) {
      if(text[sorted[i]]< text[sorted[i+1]])
        continue;
      else return false;
    }
    else if(p[sorted[i]]==p[sorted[i+1]]) {
      assert(sorted[i] < sorted[i+1]); // because of how the sorting is done
      if(text[sorted[i]]==text[sorted[i+1]])
        continue;
      else return false;
    }
    else {
      // it cannot be p[sorted[i]]> p[sorted[i+1]]
      cerr << "Logical error in strict match check:\n";
      for(unsigned j=0;j<sorted.size();j++)
        cerr << i << " " << p[i] << " " << sorted[i] << endl;
      exit(1);
    }
  }
  return true;
}


// compute the sorted vector to be used by op_match_check
void pattern_preprocess(vector<int32_t> &p, vector<int32_t> &sorted) 
{
  // build "enhanced" pattern and sort 
  vector<pair<int32_t,int32_t>> ep;
  for(unsigned i=0;i<p.size();i++)
    ep.emplace_back(p[i],i);
  sort(ep.begin(),ep.end());
  // check (you never know)
  for(unsigned i=0;i<p.size()-1;i++)
    if(ep[i].first<ep[i+1].first)
      continue;
    else assert(ep[i].first==ep[i+1].first and ep[i].second <  ep[i+1].second);
  // copy to sorted 
  for(unsigned i=0;i<ep.size();i++)
    sorted.push_back(ep[i].second);
  // check again
  assert(op_match_check(&p[0],sorted));
}



// store the integers from a single line into a vector 
void line2vector(string& line, vector<int32_t> &p)
{
    int32_t i;
    istringstream iss(line);
    while(true) {
      if(!(iss>>i)) break;
      p.push_back(i);
    }
}
