#!/usr/bin/env python3
import subprocess, os.path, sys, argparse, time, struct


Description = """Tool to create a table reporting the results of various search experiments.
The tool can work in three different modes
 1. If option -c (check) is used the number and position of pattern 
    occurrences are written to a file and compared to the output of
    the brute force algorithm. The output table only contains "ok"
    entries. If a mismatch occurs the script stops with an error
 2. If option -f (falsep) is given (and not -c) the tool reports
    the number of phase 1 and phase 2 false positives 
 3. If neither -c or -f are given, the tool reports the average 
    running time for the search operation and includes the running
    time for the brute force search (scan) as a comparison
    If option --fmop is given it also reports average running times 
    for the fmop algorithm; if also option --sso is given the tool reports 
    also the average times for the sso algorithm. Warning: sso (and 
    consequently the script) will fail if the input file contains values 
    outside the -127,127 range.

See function myalgo_test for further details

Note: tables are built reading running times, #occ's and so on from files 
produced by makefiles. If the patterns do not change algorithms are not 
rerun. To run new experiments change the pattern file or delete all the    
      .locate .slocate .bforce .sbforce   (check output correctness)  
      .time .stime .bf .sbf               (measure running time)
"""

Latex_name = {'csa':"\\csa", 'smi':"\\smi", 'bf':"\\scan",
              'sso':"\\sso",'fmop':"\\fmo",
              'lsd':"\\lsd", 'lsk':"\\lsk", 'dlt':"\\dlt",
              'mix':"\\mix", 'mid':"\\mixd"}

# Separators for a Latex table
ColSep = "&"
RowEnd =" \\\\\\hline\n"
# Separators for a text table  
#ColSep = ";"
#RowEnd = "\n"


# if check==True 
#   verify that smsearch returns exactly the same patterns as brute force 
#   output table contains only "ok" entries
#   the setting falsep==True is ignored
# if check==False 
#   if falsep==True
#     report the number of False positives for each real occurrence of the pattern
#   if falsep==False
#     report the average number of milliseconds for each pattern search
def myalgo_test(wsize,bsize,files,basedir,enc,check,pnum,plen,falsep,strict):
  # process all test files
  csa = []
  time = []; s3 = []; s2=[]; s1=[]; timeocc = []
  for f in files:
    fname = os.path.join(basedir,f)
    if check:
      target = "search_scheck" if strict else "search_check"
      command = "make  %s BASE=%s WSIZE=%s BSIZE=%s ENC=%s" % (target,fname,wsize,bsize,enc)
      command += " PNUM=%d PLEN=%d" %(pnum,plen)
      subprocess.check_call(command.split(),stdout=sys.stderr)
      # note: make exists with status 2 if there is an error 
      # if the patterns found by smi and bruteforce are different, the cmp
      # command fails and so does the make, and check_call raise a CalledProcess error
      csa.append("ok")
    else:
      # run smsearch to measure running time 
      target = "search_stime" if strict else "search_time"
      target_file = "smsearch.stime" if strict else "smsearch.time"
      command = "make  %s BASE=%s WSIZE=%s BSIZE=%s ENC=%s" % (target,fname,wsize,bsize,enc)
      command += " PNUM=%d PLEN=%d" %(pnum,plen)
      subprocess.check_call(command.split(),stdout=sys.stderr)
      with open(target_file,"r") as f:
        s = f.readline()        
      values = s.split()
      s3.append(int(values[1]))  # step 3: real occurrences 
      s2.append(float(values[3])/s3[-1])  # step 2: order match for the full window 
      s1.append(float(values[5])/s3[-1])  # step 1: exact order match for the window tail 
      time.append(1000*float(values[7])/pnum) # millisecond per pattern
      timeocc.append(1000000*float(values[7])/s3[-1]) # microsecond per occ (not used)
  t = ""
  if check:      
    t += "%-7s  %2s %2s" % (Latex_name['smi']+"-"+Latex_name[enc],wsize,bsize)
    for i in range(len(csa)):
      t += " %s %-5s" % (ColSep, csa[i])
    t += RowEnd
  else:
    if falsep:
      # average # of occurrences 
      t += "%-4s $q\\!=\\!%s, B\\!=\\!%s$ aocc" % (Latex_name[enc],wsize,bsize)
      t += "".join([" %s %6.2f"%(ColSep,i/pnum)  for i in s3])
      t += RowEnd
      t += "%-7s  %2s %2s s1" % (Latex_name['smi']+"-"+Latex_name[enc],wsize,bsize)
      t += "".join([" %s %6.2f"%(ColSep,i)  for i in s1])
      t += RowEnd
      t += "%-7s  %2s %2s s2" % (Latex_name['smi']+"-"+Latex_name[enc],wsize,bsize)
      t += "".join([" %s %6.2f"%(ColSep,i)  for i in s2])
      t += RowEnd
    else:
      # tot # of occurrences 
      t += "%-4s $q\\!=\\!%s, B\\!=\\!%s$  occ" % (Latex_name[enc],wsize,bsize)
      t += "".join([" %s %6d"%(ColSep,i)  for i in s3])
      t += RowEnd
      # running time millseconds x pattern    
      t += "%-4s $q\\!=\\!%s, B\\!=\\!%s$ time" % (Latex_name[enc],wsize,bsize)
      t += "".join([" %s %6.2f"%(ColSep,i)  for i in time])
      t += RowEnd
      # time/occ microseconds X occ       
      #t += "%-4s $q\\!=\\!%s, B\\!=\\!%s$ t/oc" % (Latex_name[enc],wsize,bsize)
      #t += "".join([" %s %6.2f"%(ColSep,i)  for i in timeocc])
      #t += RowEnd        
  return t  


# create the row relative to bruteforce search 
def make_bf(model,files,basedir,pnum,plen,strict):
  x = model.split(ColSep)
  c1 = len(x[0])
  c2 = len(x[1])
  # process all test files
  gz = []
  gocc = []
  target_ext = ".sbf" if strict else ".bf"
  for f in files:
    fname = os.path.join(basedir,f)
    patname = "%s.pat.%s.%s" % (fname,pnum,plen) 
    # taking advantage of the fact that the patfile already exists
    command = "make %s%s BASE=%s" %(patname,target_ext,fname)
    subprocess.check_call(command.split(),stdout=sys.stderr)
    with open(patname+target_ext,"r") as g:
      x = g.readline()
    gz.append(float(x.split()[7]))
    gocc.append(int(x.split()[0]))
  d = "%-*s" % (c1, Latex_name['bf']+" millisecXpat") 
  for i in range(len(gz)):
    d += "&%*.2f " % (c2-1,gz[i])  
  dz = d[:-1] + RowEnd
  d = "%-*s" % (c1, Latex_name['bf']+" occ") 
  for i in range(len(gocc)):
    d += "&%*d " % (c2-1,gocc[i])
  docc = d[:-1] + RowEnd  
  return docc + dz

# create the row relative to sso/fmop search 
def make_sso(model,files,basedir,pnum,plen,alg):
  x = model.split(ColSep)
  c1 = len(x[0])
  c2 = len(x[1])
  # process all test files
  gz = []
  gocc = []
  target_ext = "." + alg
  for f in files:
    fname = os.path.join(basedir,f)
    patname = "%s.pat.%s.%s" % (fname,pnum,plen) 
    # taking advantage of the fact that the patfile already exists
    command = "make %s%s BASE=%s" %(patname,target_ext,fname)
    subprocess.check_call(command.split(),stdout=sys.stderr)
    with open(patname+target_ext,"r") as g:
      x = g.readline()
    gz.append(float(x.split()[7]))
    gocc.append(int(x.split()[0]))
  d = "%-*s" % (c1, Latex_name[alg]+" millisecXpat") 
  for i in range(len(gz)):
    d += "&%*.2f " % (c2-1,gz[i])  
  dz = d[:-1] + RowEnd
  d = "%-*s" % (c1, Latex_name[alg]+" occ") 
  for i in range(len(gocc)):
    d += "&%*d " % (c2-1,gocc[i])
  docc = d[:-1] + RowEnd  
  return docc + dz


def make_names(model,files):
  x = model.split(ColSep)
  c1 = len(x[0])
  c2 = len(x[1])
  row = " "*c1
  for f in files:
    row += "%s %-*s" % (ColSep, c2-1,f)
  if row[-1]==" ":   
    row = row[:-1]
  return row + RowEnd
  
# check that all files in flist exist in directory basename
def check_files(flist, basename):
  for f in flist:
    fname = os.path.join(basename,f)
    assert os.path.exists(fname), "Unknown test file: " + fname

# check that all encoders are known, ie exist in Latex_name dictionary
def check_encoders(encoders):
  for e in encoders:
    if not e in Latex_name:
      assert False, "Unknown encoder: " + e

def main():
  # show_command_line(sys.stderr)
  parser = argparse.ArgumentParser(description=Description)
  parser.add_argument('files', help='test files separated by colons', type=str)
  parser.add_argument('-w', '--window_size', help='sizes of sliding window (def 4:8)', default='4:8', type=str)  
  parser.add_argument('-b', '--block_size', help='sizes of block (def 32:64)', default='32:64', type=str)  
  parser.add_argument('-d', '--base_dir', help='base directory (def .)', default='.', type=str)
  parser.add_argument('-e', '--enc', help='encoder for delta files (def lsk)', default='lsk', type=str)
  parser.add_argument('-p', '--patterns', help='#patterns  (def. 100)',default=100, type=int)
  parser.add_argument('-l', '--length', help='length of each pattern (def. 20)',default=20, type=int)
  parser.add_argument('-f', '--falsep', help='report false positives (def No)',action='store_true')  
  parser.add_argument('-c', '--check', help='compare with bruteforce (def No)',action='store_true')
  parser.add_argument('-s', '--strict', help='search in strict mode (def No)',action='store_true')
  parser.add_argument('--sso', help='test sso algorithm (def No)',action='store_true')
  parser.add_argument('--fmop', help='test fmop algorithm (def No)',action='store_true')
  args = parser.parse_args()  
  # run the algorithm   
  s1 = time.time()
  # multiple test file names, window sizes and block sizes separated by colon
  ifiles = args.files.split(":")
  wsizes = args.window_size.split(":")
  bsizes = args.block_size.split(":")
  encoder = args.enc                 # a single encoder is allowed 
  check_files(ifiles,args.base_dir)
  check_encoders([encoder])
  table = ""
  for b in bsizes:
    for w in wsizes:
      r = myalgo_test(w,b,ifiles,args.base_dir,encoder,args.check,args.patterns,args.length,args.falsep,args.strict)
      table += r
  # the model row is the first row up to the "\\" sequence    
  model_row = table.split("\n")[0].split("\\\\")[0]
  # create row with name of the test files
  table = make_names(model_row, ifiles) + table
  if (not args.check) and (not args.falsep):
    if args.sso:
      table = table + make_sso(model_row,ifiles,args.base_dir,args.patterns,args.length,"sso")
    if args.fmop:
      table = table + make_sso(model_row,ifiles,args.base_dir,args.patterns,args.length,"fmop")
    table = table + make_bf(model_row,ifiles,args.base_dir,args.patterns,args.length,args.strict)
  table = "%% Pnum=%d, Plen=%d\n" % (args.patterns,args.length) + table
  e1 = time.time()
  print("Elapsed time: %.3f\n" % (e1-s1),file=sys.stderr)
  print(table)
  exit(0)

  
if __name__ == '__main__':
    main()
 
