/* bruteforce.cpp
 * bruteforce algorithm for the order-preserving pattern matching problem
 * Copyright 2015-2018 Giovanni Manzini
 * */ 

#include <stdint.h>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include "opmatch.hpp"


using namespace std;


struct BruteForceError: public std::runtime_error
{
    BruteForceError(std::string const& message)
        : std::runtime_error(message)
    {}
};



int32_t bf_search(vector<int32_t> &text, vector<int32_t> &p, vector<int64_t> &pos, int verbose)
{
  // show pattern
  if(verbose>1) {
    cerr << "--- ";
    for(unsigned i=0;i<p.size();i++)
      cerr << p[i] << " ";
    cerr << "---\n";
  }
  // derive sorted order
  vector<int32_t> sorted;
  pattern_preprocess(p, sorted);
  // go brute force
  int32_t tot = 0;
  for(int64_t j=0;j+p.size() <= text.size();j++) {
    // check t[j:]
    if(op_match_check(&text[j],sorted)) {
      pos.push_back(j); tot++;
      if(verbose>1) {
        cerr << "[" << j << "]\n";
        for(unsigned i=0;i<p.size();i++)
          cerr << text[j+i] << " ";
        cerr << endl;
      }
    }
  }
  if(verbose==1) {
    fprintf(stderr,"Occ: %-8d", tot);
    for(unsigned i=0;i<p.size();i++) fprintf(stderr," %d", p[i]);
    fprintf(stderr,"\n");    
  }
  return tot;
}

int32_t bf_strict_search(vector<int32_t> &text, vector<int32_t> &p, vector<int64_t> &pos, int verbose)
{
  // show pattern
  if(verbose>1) {
    cerr << "--- ";
    for(unsigned i=0;i<p.size();i++)
      cerr << p[i] << " ";
    cerr << "---\n";
  }
  // derive sorted order
  vector<int32_t> sorted;
  pattern_preprocess(p, sorted);
  // go brute force
  int32_t tot = 0;
  for(int64_t j=0;j+p.size() <= text.size();j++) {
    // check t[j:]
    if(op_strict_match_check(&text[j],p, sorted)) {
      pos.push_back(j); tot++;
      if(verbose>1) {
        cerr << "[" << j << "]\n";
        for(unsigned i=0;i<p.size();i++)
          cerr << text[j+i] << " ";
        cerr << endl;
      }
    }
  }
  if(verbose==1) {
    fprintf(stderr,"Occ: %-8d", tot);
    for(unsigned i=0;i<p.size();i++) fprintf(stderr," %d", p[i]);
    fprintf(stderr,"\n");    
  }
  return tot;
}



// read a vector of int32_t little endian format
void read_text(string const& name,vector<int32_t>& t)
{
  uint8_t buffer[4];
  ifstream f(name,ios::in|ios::binary);
  if(!f.is_open())
      throw BruteForceError("Unable to open input file " + name);
  while(true) {
    f.read((char *) buffer,4);
    if(f.gcount()==0) break;
    assert(f.gcount()==4);
    int32_t v=0;
    for(int i=0;i<4;i++)
      v |= buffer[i]<<(8*i);
    t.push_back(v);
  }
}

void read_patterns(string& pname, vector<vector<int32_t>>& pats)
{
  ifstream f(pname,ios::in); 
  if(!f.is_open())
      throw BruteForceError("Unable to open pattern file " + pname);
  string line;
  while(getline(f,line)) {
    vector<int32_t> p;
    line2vector(line,p);
    pats.push_back(p);
  }
  f.close();
}



void usage(char *name)
{
    cerr << "Usage " << name << " text_file pattern_file [outfile] [-hsv]" << endl;
    cerr << "    Order Preserving search in TEXT_FILE of the patterns in PATTERN_FILE" << endl;
    cerr << "    If OUTFILE is specified the positions of the OP matches are written there" << endl;
    cerr << "    Options:\n";
    cerr << "      -h   This help message\n";
    cerr << "      -s   Strict pattern matching (handles equal values)\n";
    cerr << "      -v   Verbose: show matching substrings to stderr\n";
    exit(1);
}


int main(int argc, char** argv) {
  int c;
  bool strict = false;
  int verbose = 0;
  // ----------- parse options 
  while ((c = getopt (argc, argv, "hsv")) != -1)
    switch (c) {
      case 's':
        strict = true;
        break;
      case 'v':
        verbose++;
        break;
      case 'h':
      default:
        usage(argv[0]);
    }
  // --- check mandatory args are there 
  argc -= (optind -1);  // get rid of optional arguments  
  if(argc!=3 and argc!=4) 
    usage(argv[0]);
  argv += optind-1;
  // --------- parse mandatory args 
  string infile(argv[1]);
  string pname(argv[2]);
  bool save_locations = (argc>3); // if oufile is given
  ofstream outf;
  if(save_locations)
    outf.open(argv[3]);
  // read sequence data   
  vector<int32_t> text;
  read_text(argv[1],text);
  cerr << "Input file contains " << text.size() << " values\n";
  // read patterns to be searched
  vector<vector<int32_t>> pats;
  read_patterns(pname,pats);  
  // main loop
  int64_t tot_found = 0;
  vector<int64_t> pos;
  clock_t t=clock();
  for(unsigned i=0;i<pats.size();i++) {
    pos.clear();
    // brute force search
    if (strict)
      tot_found += bf_strict_search(text,pats[i],pos,verbose);
    else
      tot_found += bf_search(text,pats[i],pos,verbose);
    if(save_locations) {
      // write matching positions for p[] on a single output line  
      for(unsigned i=0;i<pos.size();i++)
        outf << pos[i] << " ";
      outf << endl;
    }
  }
  t = clock() - t;
  if(save_locations)
    outf.close();
  cout << tot_found << " OP matches found.";
  cout << " Elapsed " << ((double) t)/CLOCKS_PER_SEC << " seconds ";
  cout << (1000.0/pats.size())*((double) t)/CLOCKS_PER_SEC << " millisecXpat\n";
  return 0;
}
