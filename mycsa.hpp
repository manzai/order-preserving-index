// definition of the compressed index type used by
// makecsa and smsearch

using namespace sdsl;

//most space economical still supporting O(1) rank  
//typedef wt_huff<bit_vector,rank_support_v5<>,select_support_scan<>,select_support_scan<>> my_wt;

// interleaved representation: good tradeoff between space and rank cost
typedef wt_huff<bit_vector_il<>,rank_support_il<>> my_wt; 
typedef csa_wt<my_wt, TEXT_DENS, TEXTI_DENS, text_order_sa_sampling<>> my_csa;
