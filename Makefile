# ---- if necessary provide the location of library/include files for sdsl-lite
LIB_DIR = 
INC_DIR = 
# ---- general compilation options and flags --------------------
MY_CXX_FLAGS= -std=c++11 -Wall -Wextra -DNDEBUG
MY_CXX_OPT_FLAGS= -O3 -ffast-math -funroll-loops -msse4.2
CXX_FLAGS=$(MY_CXX_FLAGS) $(MY_CXX_OPT_FLAGS) -I$(INC_DIR) -L$(LIB_DIR) 
MY_CXX=/usr/bin/c++
CCLIB=-lsdsl -ldivsufsort -ldivsufsort64 

# ---- default smi parameters -----------------------
# default text density is 32
BSIZE=32
# default window size (q in the paper) is 6
WSIZE=6
# default encoder: log skewed
ENC=lsk
EXECS=makecsa.$(BSIZE).x smsearch.$(BSIZE).x bruteforce.x others/fmop_main others/sso_main
INDEX_FLAGS=-DTEXT_DENS=$(BSIZE) -DTEXTI_DENS=16777216
# default decompressor: c++
D_EXT=dzi
E_EXT=ezi

all: $(EXECS)

.PHONY: others/fmop_main others/sso_main clean zipfile

# create makecsa/smsearch with a given block size
makecsa.$(BSIZE).x: makecsa.cpp mycsa.hpp
	$(MY_CXX) $(CXX_FLAGS) $(INDEX_FLAGS) -o $@ $< $(CCLIB)

smsearch.$(BSIZE).x: smsearch.cpp smindex.hpp mycsa.hpp delta.hpp mybits.hpp slidingWindow.hpp opmatch.hpp
	$(MY_CXX) $(CXX_FLAGS) $(INDEX_FLAGS) -o $@ smsearch.cpp $(CCLIB)

# bruteforce search (aka scan in the paper)
bruteforce.x: bruteforce.cpp opmatch.hpp
	$(MY_CXX) $(CXX_FLAGS) -o $@ $<

# compilation of sso_main
others/sso_main:
	make -C others sso_main
  
# compilation of fmop_main
others/fmop_main:
	make -C others fmop_main
  
# zip file of smi source code
zipfile:
	zip smi.zip bruteforce.cpp  makecsa.cpp smsearch.cpp *hpp\
            eqsplit.py xosplit.py\
            deltacomp.py mybits.py patextract.py smbuild.py Makefile 

# --- delete executables
clean:
	rm -f makecsa.*.x smsearch.*.x bruteforce.x
	make -C others clean

# --- delete compressed files
index_clean:
	rm -f $(BASE).*.dz $(BASE).*.ez $(BASE).*.dzi $(BASE).*.ezi $(BASE).*.csa\
	      $(BASE).*.xo $(BASE).*.yo $(BASE).*.xd $(BASE).*.yd


# ---------------------------------------------------------------
# rules for the construction of the strict index (.csa + .ez files)
# (explicitly considers equal values)
# Use the command line:
#    make index_strict BASE=basefilename WSIZE=wsize BSIZE=bsize ENC=encoder
# to build the strict index, or
#    make index_scheck BASE=basefilename WSIZE=wsize BSIZE=bsize ENC=encoder
# if you also want to check decompression 

# build strict index
index_strict: $(BASE).$(WSIZE).yo.$(BSIZE).csa \
              $(BASE).$(ENC).$(WSIZE).$(BSIZE).ez

# build strict index and check correctness by decompressing the index 
# also compress with gzip and xz to have a reference (can be safely deleted)
index_scheck: $(BASE).$(WSIZE).yo.$(BSIZE).csa \
             $(BASE).$(ENC).$(WSIZE).$(BSIZE).ez \
             $(BASE).$(ENC).$(WSIZE).$(BSIZE).scheck $(BASE).gz $(BASE).xz

# avoid deletion of xo and yo files
.SECONDARY: $(BASE).$(WSIZE).xo  $(BASE).$(WSIZE).yo

# makecsa.B.x creates the csa with block size B
%.$(BSIZE).csa: % makecsa.$(BSIZE).x
	makecsa.$(BSIZE).x $* 

# deltacomp --strict creates the ez file
%.$(ENC).$(WSIZE).$(BSIZE).ez: % %.$(WSIZE).yo
	python3 deltacomp.py --strict $* $*.$(WSIZE).yo -b $(BSIZE) -e $(ENC)

# eqsplit creates the yo file (uncompressed order component)
%.$(WSIZE).yo: %
	python3 eqsplit.py $* -w $(WSIZE) 

# python decompress, for testing purposes 
%.ezd: %.ez $(BASE).$(WSIZE).yo
	python3 deltacomp.py --strict -d $< $(BASE).$(WSIZE).yo

# c++ decompress, significantly faster
%.ezi: %.ez $(BASE).$(WSIZE).yo smsearch.$(BSIZE).x
	smsearch.$(BSIZE).x  -s -v $(BASE) $(WSIZE) $(ENC) 

# default decompressor is c++; set E_EXT=ezd to use python decompr.
#E_EXT=ezd

# check decompression of delta values with c++ or python decompressor 
%.scheck: $(BASE) %.$(E_EXT)
	cmp $(BASE) $*.$(E_EXT) > $@



# ---------------------------------------------------------------
# rules for the construction of the non-strict index (.csa + .dz files)
# Use the command line:
#    make index BASE=basefilename WSIZE=wsize BSIZE=bsize ENC=encoder
# to build the index, or
#    make index_check BASE=basefilename WSIZE=wsize BSIZE=bsize ENC=encoder
# if you also want to check decompression

# build index
index: $(BASE).$(WSIZE).xo.$(BSIZE).csa $(BASE).$(ENC).$(WSIZE).$(BSIZE).dz

# build index and check correctness by decompressing the index 
index_check: $(BASE).$(WSIZE).xo.$(BSIZE).csa \
             $(BASE).$(ENC).$(WSIZE).$(BSIZE).dz \
             $(BASE).$(ENC).$(WSIZE).$(BSIZE).check $(BASE).gz $(BASE).xz

# deltacomp creates the dz file
%.$(ENC).$(WSIZE).$(BSIZE).dz: % %.$(WSIZE).xo
	python3 deltacomp.py $* $*.$(WSIZE).xo -b $(BSIZE) -e $(ENC)

# xosplit creates the xo file (uncompressed order component)
%.$(WSIZE).xo: %
	python3 xosplit.py $* -w $(WSIZE) 

# python decompress, for testing purposes 
%.dzd: %.dz $(BASE).$(WSIZE).xo
	python3 deltacomp.py -d $< $(BASE).$(WSIZE).xo

# c++ decompress
%.dzi: %.dz $(BASE).$(WSIZE).xo smsearch.$(BSIZE).x
	smsearch.$(BSIZE).x  -v $(BASE) $(WSIZE) $(ENC)

# default decompressor is c++; set D_EXT=dzd to use python decompr.
#D_EXT=dzd

# check decompression of delta values with c++ or python decompressor
%.check: $(BASE) %.$(D_EXT)
	cmp $(BASE) $*.$(D_EXT) > $@

# ---------------------------------------------------------
# compare smsearch with bruteforce (compare position reported occurrences)
PNUM=100
PLEN=20
FULLPFILE=$(BASE).pat.$(PNUM).$(PLEN)
PFILE=$(notdir $(FULLPFILE))
# create file of patterns extracting them from BASE
$(BASE).pat.$(PNUM).$(PLEN): $(BASE)
	python3 patextract.py $(BASE) -p $(PNUM) -l $(PLEN) > $@


# strict index
# example: make search_scheck BASE=collections/ibm
search_scheck: $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).slocate\
              $(BASE).$(PFILE).sbforce
	cmp $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).slocate \
        $(BASE).$(PFILE).sbforce > $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).scheck

# execute strict brute force search
$(BASE).$(PFILE).sbforce: bruteforce.x $(BASE) $(FULLPFILE)
	bruteforce.x -s $(BASE) $(FULLPFILE) $(BASE).$(PFILE).sbforce

# smsearch strict with output to .slocate file
%.$(ENC).$(WSIZE).$(BSIZE).$(PFILE).slocate: smsearch.$(BSIZE).x $(FULLPFILE)\
        %.$(WSIZE).yo.$(BSIZE).csa %.$(ENC).$(WSIZE).$(BSIZE).ez
	smsearch.$(BSIZE).x -s $(BASE) $(WSIZE) $(ENC) $(FULLPFILE) $@


# non-strict index
# example: make search_check BASE=collections/ibm
search_check: $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).locate\
              $(BASE).$(PFILE).bforce
	cmp $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).locate \
        $(BASE).$(PFILE).bforce > $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).check

# brute force search with output to .bforce file
$(BASE).$(PFILE).bforce: bruteforce.x $(BASE) $(FULLPFILE)
	bruteforce.x $(BASE) $(FULLPFILE) $(BASE).$(PFILE).bforce

# smsearch with output to .locate file
%.$(ENC).$(WSIZE).$(BSIZE).$(PFILE).locate: smsearch.$(BSIZE).x $(FULLPFILE)\
        %.$(WSIZE).xo.$(BSIZE).csa %.$(ENC).$(WSIZE).$(BSIZE).dz
	smsearch.$(BSIZE).x $(BASE) $(WSIZE) $(ENC) $(FULLPFILE) $@


# ---------------------------------------------------------
# measure running time of smsearch vs bruteforce vs fmop vs sso

# strict index
# example: make search_stime BASE=collections/ibm
search_stime: $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).stime\
              $(FULLPFILE).sbf
	cp $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).stime smsearch.stime

# smsearch -s output to file .stime  (no positions only time and occ)
%.$(ENC).$(WSIZE).$(BSIZE).$(PFILE).stime: smsearch.$(BSIZE).x $(FULLPFILE)\
        %.$(WSIZE).yo.$(BSIZE).csa %.$(ENC).$(WSIZE).$(BSIZE).ez
	smsearch.$(BSIZE).x -s $(BASE) $(WSIZE) $(ENC) $(FULLPFILE) > $@

# execution of bruteforce strict (only time and occ)
%.sbf: % $(BASE) bruteforce.x
	bruteforce.x -s $(BASE) $* > $@

# execution of sso (only time and occ)
%.sso: % $(BASE) others/sso_main
	others/sso_main $(BASE) $* > $@

# execution of fmop (only time and occ)
%.fmop: % $(BASE) others/fmop_main
	others/fmop_main $(BASE) $* > $@


# non-strict index
# example: make search_time BASE=collections/ibm
search_time: $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).time\
             $(FULLPFILE).sbf
	cp $(BASE).$(ENC).$(WSIZE).$(BSIZE).$(PFILE).time smsearch.time

# smsearch with output to .time file (no positions only time and occ)
%.$(ENC).$(WSIZE).$(BSIZE).$(PFILE).time: smsearch.$(BSIZE).x $(FULLPFILE)\
        %.$(WSIZE).xo.$(BSIZE).csa %.$(ENC).$(WSIZE).$(BSIZE).dz
	smsearch.$(BSIZE).x $(BASE) $(WSIZE) $(ENC) $(FULLPFILE) > $@

# execution of bruteforce (only time and occ)
%.bf: % $(BASE) bruteforce.x
	bruteforce.x $(BASE) $* > $@


# ----- gzip and xz compression ------------

# gzip compression
%.gz: %
	gzip --best $* -c > $*.gz

# xz compression
%.xz: %
	xz --best $* -c > $@

