#include <iostream>
#include <fstream>
#include <string>

using namespace std;

// constants defining the type of data to be encoded
#define Minimal 0
#define Intermediate 1
#define Maximal 2
#define Order 3

class BitDecoder
{
  protected:
  uint8_t *b;
  uint64_t next; 
  int abits;      // available bits in next

  // get k bits from the buffer
  uint64_t get_bits(int k) {
    assert(k<=64 and k>=0);
    if(abits==0) {   // fill buffer if empty
      next = *b++;
      abits = 8;
    }
    uint64_t v=0;    // v is output value
    int written = 0; // bits written to v
    while(k>= abits) {
      v |= next<<written;  // write abits bit
      written += abits;
      k -= abits;
      next = *b++;
      abits = 8;
    }
    if(k==0) return v;
    int64_t mask = (1u<<k) -1; // mask consisting of k ones
    v |= (next&mask) << written;
    next >>= k;
    abits -= k;
    return v;
  }
  
  // init the decoder providing the buffer
  public:
  void init(uint8_t *buffer) {
    b = buffer;
    next=0;
    abits = 0;
  }

  // abstract method to decode a single value
  virtual uint64_t decode(uint64_t n, int type) = 0;
  virtual ~BitDecoder() {} 
};
  

// decode a value t, 0 <= t < n using delta coding 
// the size of the encoding is bounded by 
//   2bin(t+1) + 1 \approx 2 log(t)
// where bin(i) is the number of bits o integer i   
class BitDecoderdlt : public BitDecoder
{
  // decode a value t, 0 <= t < n 
  uint64_t decode(uint64_t n, int type) {
    (void)n, (void) type;
    const uint64_t one=1;
    int i=0;
    while(get_bits(1)==0) i++;
    if(i==0) return 0;
    uint64_t v = get_bits(i);
    assert (v < (one<<i));
    return   (v | (one<<i)) -1;
  }
  
};


// decode a value t, 0 <= t < n using log-skewed encoding  
// the size of the encoding is bounded by 
//   bin(n-1) = \approx log(n) 
// where bin(i) is the number of bits o integer i 
class BitDecoderlsk : public BitDecoder
{

  // iterative stand-alone log-skewed decoding 
  uint64_t log_skewed_rec(uint64_t n) {
    if(n==1) return 0;
    int log2=1; 
    uint64_t d, pot2=2;
    while(pot2 < n) {
      pot2 *=2; log2 +=1;
    }
    if(pot2==n) 
      return get_bits(log2);
    else {
      d = n - pot2/2;
      if(get_bits(1)!=0) {
        // LSB==1
        assert(log2>1);
        return   d + get_bits(log2-1);
      }
      else { 
        //LSB ==0
        return log_skewed_rec(d);
      }
    }    
  }

  // decode a value t, 0 <= t < n 
  uint64_t decode(uint64_t n, int type) {
    (void) type;
    // iterative version of log-skewed decoding 
    int log2;
    uint64_t d, pot2;
    while(true) {
      if(n==1) return 0;
      log2=1; pot2=2;
      while(pot2 < n) {
        pot2 *=2; log2 +=1;
      }
      if(pot2==n) 
        return get_bits(log2);
      else {
        d = n - pot2/2;
        if(get_bits(1)!=0) {
          // LSB==1
          assert(log2>1);
          return   d + get_bits(log2-1);
        }
      }
      n = d; // recursion
    }    
  }
    
};


// decode a value t, 0 <= t < n using log skewed encoding for representing
// the number of bits of t+1 followed by the bits of t+1 (except the MSB) 
// the size of the encoding is bounded by 
//   bin(bin(n)-1) + bin(t+1)-1 \approx log log n + log t 
// where bin(i) is the number of bits o integer i 
class BitDecoderlsd : public BitDecoder
{

  // iterative stand-alone log-skewed decoding 
  uint64_t log_skewed_iter(uint64_t n) {
    int log2;
    uint64_t d, pot2;
    while(true) {
      if(n==1) return 0;
      log2=1; pot2=2;
      while(pot2 < n) {
        pot2 *=2; log2 +=1;
      }
      if(pot2==n) 
        return get_bits(log2);
      else {
        d = n - pot2/2;
        if(get_bits(1)!=0) {
          // LSB==1
          assert(log2>1);
          return   d + get_bits(log2-1);
        }
      }
      n = d; // recursion
    }    
  }

  // decode a value t, 0 <= t < n 
  uint64_t decode(uint64_t n, int type) {
    (void) type;
    if(n==1) return 0;
    // this is delta-like coding so we decode a value t+1 such that 0 < t+1 <= n
    // compute # of digits in n>1
    const uint64_t one=1;
    int log2n=2; uint64_t pot2n=4;
    while(pot2n <= n) {
      pot2n *= 2; log2n += 1;
    }
    // now log2n is # of bits in n (2,3->2, 4,5,..7-> 3  etc)
    // since the MSB is 1 the value t has 0 <= logt < log2n interesting digits   
    int log2t = (int) log_skewed_iter(log2n);
    // get log2t bits fromstream  
    uint64_t v = (log2t==0) ? 0 : get_bits(log2t);
    // add the MSB in front of the log2t bits 
    assert (v < (one<<log2t));
    // remove 1 to go from from t+1 to t 
    return   (v | (one<<log2t)) -1;
  }
  
};


// decode a value t, 0 <= t < n using log skewed encoding for representing
// data of type Order and Intermediate and log_skewed_delta
// for representing ata of type Minimal and Maximal 
class BitDecodermix : public BitDecoder
{

  // iterative stand-alone log-skewed decoding 
  uint64_t log_skewed_iter(uint64_t n) {
    int log2;
    uint64_t d, pot2;
    while(true) {
      if(n==1) return 0;
      log2=1; pot2=2;
      while(pot2 < n) {
        pot2 *=2; log2 +=1;
      }
      if(pot2==n) 
        return get_bits(log2);
      else {
        d = n - pot2/2;
        if(get_bits(1)!=0) {
          // LSB==1
          assert(log2>1);
          return   d + get_bits(log2-1);
        }
      }
      n = d; // recursion
    }    
  }

  // decode a value t, 0 <= t < n 
  uint64_t decode(uint64_t n, int type) {
    if(type==Minimal or type==Maximal) { 
      // use lsd decoding 
      if(n==1) return 0;
      // this is delta-like coding so we decode a value t+1 such that 0 < t+1 <= n
      // compute # of digits in n>1
      const uint64_t one=1;
      int log2n=2; uint64_t pot2n=4;
      while(pot2n <= n) {
        pot2n *= 2; log2n += 1;
      }
      // now log2n is # of bits in n (2,3->2, 4,5,..7-> 3  etc)
      // since the MSB is 1 the value t has 0 <= logt < log2n interesting digits   
      int log2t = (int) log_skewed_iter(log2n);
      // get log2t bits fromstream  
      uint64_t v = (log2t==0) ? 0 : get_bits(log2t);
      // add the MSB in front of the log2t bits 
      assert (v < (one<<log2t));
      // remove 1 to go from from t+1 to t 
      return   (v | (one<<log2t)) -1;
    }
    // for Intermediate and Order use log-skewed
    else if(type==Intermediate or type==Order) 
      return log_skewed_iter(n);
    // there are no other cases   
    assert(false);
    return 0;
  }
  
};



class BitDecodermid : public BitDecoder
{

  // iterative stand-alone log-skewed decoding 
  uint64_t log_skewed_iter(uint64_t n) {
    int log2;
    uint64_t d, pot2;
    while(true) {
      if(n==1) return 0;
      log2=1; pot2=2;
      while(pot2 < n) {
        pot2 *=2; log2 +=1;
      }
      if(pot2==n) 
        return get_bits(log2);
      else {
        d = n - pot2/2;
        if(get_bits(1)!=0) {
          // LSB==1
          assert(log2>1);
          return   d + get_bits(log2-1);
        }
      }
      n = d; // recursion
    }    
  }

  // decode a value t, 0 <= t < n 
  uint64_t decode(uint64_t n, int type) {
    // for Minimal and Maximal use delta coding  
    if(type==Minimal or type==Maximal) { 
      const uint64_t one=1;
      int i=0;
      while(get_bits(1)==0) i++;
      if(i==0) return 0;
      uint64_t v = get_bits(i);
      assert (v < (one<<i));
      return   (v | (one<<i)) -1;
    }
    // for Intermediate and Order use log-skewed
    else if(type==Intermediate or type==Order) 
      return log_skewed_iter(n);
    // there are no other cases   
    assert(false);
    return 0;
  }
  
};

