class SlidingWindow
{
   int s,e,nel,size;  // start end num element, capacity
   int32_t *b;        // buffer
   
   public:
   SlidingWindow(int _z) : size(_z){
     assert(size>0);
     b= new int[size];
     s=e=nel=0;
   }
   ~SlidingWindow() {delete[] b;}
   // add and remove a single element
   void push(int32_t v) {
     assert(nel++ <size);   // test and updates of nel are all in assertions 
     b[e] = v;             // so the all stay or go away togther
     e = (e+1)%size;
   }
   void pop() {
     assert(nel-- >0);
     s = (s+1)%size;
   }
   // extract element in position -i (1 is the last, 2 second last)
   int32_t extract(int i) {
     assert(i<=nel);
     return b[(e-i+size)%size];
   }
   // successor: return the smallest element largest than v
   // or v if all elements are <= v
   int32_t succ(int32_t const v) {
     assert(nel>0);
     int32_t su = v;
     for(int i=s; ;) {
       if(b[i]>v) {
         if(su==v or b[i]<su)
           su = b[i];
       }
       i = (i+1) % size;
       if(i==e) break;
     }
     return su;
   }
  
   // minimum value in buffer
   int32_t min() {
     assert(nel>0);
     int32_t m = b[s];
     for(int i=(s+1)%size; i!=e; i = (i+1)%size)
       if(b[i]<m) m = b[i];
     return m;
   }
};
