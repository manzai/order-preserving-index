#include <sdsl/suffix_arrays.hpp>
#include <fstream>
#include <string>
#include "mycsa.hpp"  // definition of my_csa type

using namespace std;


void usage(char *name)
{
    cerr << "Usage:\n    " << name << " input_file [-h]" << endl;
    cerr << "    Order preserving search of patterns from PATTERN_FILE" << endl;
    cerr << "    Create a compressed suffix array (fm_index + wavelet tree)\n";
    cerr << "    for INPUT_FILE and save it with extension ." << TEXT_DENS <<".csa\n";
    cerr << "    Options:\n";
    cerr << "      -h   This help message\n";
    exit(1);
}


int main(int argc, char** argv) 
{
  // ----------- parse options 
  int c;
  while ((c = getopt (argc, argv, "h")) != -1)
    switch (c) {
      case 'h':  
      default:
        usage(argv[0]);
    }
  // --- check mandatory args are there 
  argc -= (optind -1);  // get rid of optional arguments  
  if(argc!=2) 
    usage(argv[0]);
  // ---- check input fileis accessible 
  ifstream infile(argv[1]);
  if(infile.good())
    infile.close();
  else {
    perror("Cannot access input file");
    exit(EXIT_FAILURE);
  }
  // define type of CSA and construct
  my_csa fm_index;
  construct(fm_index, argv[1], 1);
  
  cout << "Index construction complete, index uses " << size_in_mega_bytes(fm_index) << " MiB." << endl;
  size_t n = fm_index.size();
  cout << "BWT has size " << n << endl;
  // store index to file
  store_to_file(fm_index, string(argv[1])+"."+to_string(TEXT_DENS)+".csa");
  // uncomment the next 2 lines to output the graphic representation of used space
  //  ofstream out(string(argv[1])+"."+to_string(TEXT_DENS)+".csa.html");
  //  write_structure<HTML_FORMAT>(fm_index,out);
  return 0;
}
