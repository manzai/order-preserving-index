#!/usr/bin/env python3
# deltacomp.py
# Tool to (de)compress the delta component of a stock market index given 
# the raw data, the uncompressed order components (hence the window size), 
# the block size, and the encoder to be used for delta values
# Copyright 2015-2016 Giovanni Manzini
import os, os.path, sys, argparse, time, struct
import mybits


Description = """Tool to (de)compress the delta components given the input file,
the order component, the block size, and the encoder"""


"""
Compression format of the delta component. 

Compression and decompression require a copy of the ordering component
The window size is the first entry in the ordering component (that entry 
is not used since the first delta is stored exlicitly) but it is 
currently taken from the file name of the ordering component


Compressed file format:

== header
 n (4u) # blocks
 l1 l2 ... ln (n 16u) size of blocks in bytes
== body
 block_1 (l1)
 block_2 (l2)
 ...
 block_n (ln)

For each block
==header 
  v0 (4) first value
  max d+ (4u) maximum positive delta
  max d- (4u) minimum negative delta
stream of bits containing for the second entry onward:
  ordering value relative to the block
  delta value

  the ordering value is not present if the "global" ordering value
  is inside the block, hence there can be  at most window_size-2
  ordering values
     
  For all values in this stream we know in advance a range [0,k]
  to which each one belongs, so the idea is to use log-skewed coding
  or similar algorithms that take advantage of such range 
"""
  
# global map between integer encoder/decoder names and functions
Encoders  = {
  'lsk':{'e':mybits.lsz_encode_typed, 'd':mybits.lsbb_decode_typed},       # log skewed encoding
  'lsd':{'e':mybits.lsdz_encode_typed, 'd':mybits.lsdbb_decode_typed},     # log skewed/delta encoding
  'dlt':{'e':mybits.delta_encode_typed, 'd':mybits.deltabb_decode_typed},  # delta encoding
  'mix':{'e':mybits.mix_encode, 'd':mybits.mix_decode},         # mixed encoding lsk+lsd
  'mid':{'e':mybits.mid_encode, 'd':mybits.mid_decode}         # mixed encoding lsk+delta
}
# global variable indicating whether we are in strict mode (equal values considered)
Strict_mode = False
# constants defining the type of data to be encoded
Minimal=0; Intermediate=1; Maximal=2; Order=3


def compress(ifile,ofile,outfile,tsize,wsize,bsize,encoder):
  with open(ofile,"rb") as of:
    with open(ifile,"rb") as df:
      with open(outfile,"wb") as outf:
        num_blocks = (tsize+bsize-1)//bsize
        outf.write(struct.pack('<i',num_blocks))  # write # blocks
        outf.write(bytes(2*num_blocks))           # skip 2*num_blocks bytes
        block_lens = []
        # --- write to file individual blocks
        for i in range(num_blocks):
          if num_blocks< 10 or i%(num_blocks//10)==0: 
            print("Compressing block", i,file=sys.stderr)
          z = of.read(bsize)
          if len(z)!=bsize:
            if i!=num_blocks-1 or len(z)!=tsize%bsize:
               raise Exception("Illegal order file (too short)")
          # get block of raw data
          y = df.read(4*bsize)
          if len(y)!=4*len(z):
               raise Exception("Illegal delta file (too short)")
          v = [struct.unpack('<i',y[4*i:4*i+4])[0] for i in range(len(z))]
          assert len(v) == len(z), "order/values mismatch"
          if Strict_mode:
            cbsize = compress_block_strict(v,z,wsize,outf,encoder)
          else:
            cbsize = compress_block(v,z,wsize,outf,encoder)
          block_lens.append(cbsize)
        print(num_blocks, " blocks compressed",file=sys.stderr)
        # --- write to file the length of the blocks 
        assert len(block_lens)==num_blocks
        outf.seek(4) # skip first 4 bytes used for # blocks 
        for i in block_lens:
          outf.write(struct.pack('<H',i)) # block length as uint16_t
  # -- all done; return size of output file in bytes 
  return sum(block_lens) + 2*num_blocks + 4
    

# compress a single block
def compress_block(values,order,wsize,outf,encoder):
  newo = []
  newo_type = []  # 0: minimal, 1: intermediate, 2: maximal
  delta = []
  buffer = [values[0]]
  for v in values[1:]:
    # code for v is the position of the predecessor in the list
    # we scan the list right to left (new to old) 
    code = wsize     # means: predecessor of v not yet found
    succval = v      # means: successor of v not yet found  
    assert 0 < len(buffer)<wsize, "Illegal buffer"
    for i in range(1,len(buffer)+1):
      if buffer[-i] <= v and (code==wsize or buffer[-i]>buffer[-code]):
        code = i   # note: the closest equal value is the predecessor
      elif buffer[-i] > v and (succval==v or buffer[-i]<succval): 
        succval = buffer[-i]
    # --- code and succval computed    
    assert 0 < code<=wsize, "Illegal code computation"
    assert succval >= v, "Illegal successor computation"
    assert code==wsize or succval==v or succval > buffer[-code]
    newo.append(code)
    if code==wsize:       # minimal value
      delta.append(min(buffer) - v)
      newo_type.append(0)
    elif succval == v:      # maximal value  
      delta.append(v-buffer[-code])
      newo_type.append(2)     
    else:                       # intermediate
      delta.append((v-buffer[-code],succval-buffer[-code])) 
      newo_type.append(1)
    # update buffer inserting new value as last element
    buffer.append(v)
    if len(buffer)>=wsize:
      assert len(buffer)==wsize
      buffer.pop(0) # remove first (==oldest) element      
  # --- delta values computed. now get maxpos_delta and maxneg_delta
  assert len(delta)==len(newo_type) and  len(delta)==len(newo)
  assert len(delta) + 1 == len(order) 
  max_posdelta = max([delta[i] for i in range(len(delta)) if newo_type[i]==2]+[0])
  max_negdelta = max([delta[i] for i in range(len(delta)) if newo_type[i]==0]+[0])
  # --- output block header
  outf.write(struct.pack('<i',values[0]))          # first value
  outf.write(struct.pack('<I',max_posdelta))  
  outf.write(struct.pack('<I',max_negdelta))
  # --- output "missing" ordering values  
  outbuffer = mybits.BitBufferOut(outf,encoder)
  for i in range(len(newo)):
    if newo[i]!=order[i+1]:
      assert i<wsize-2, "Error 1 at order %d: %d vs %d" % (i, newo[i],order[i+1]) # from position wsize-2 it is newo[i]==order[i+1]  
      assert order[i+1]!=wsize and order[i+1]>i+1, "Error 2 at order %d: %d vs %d" % (i, newo[i],order[i+1])
      # newo[i] can be wsize (encoded with 0) or an integer in [1..i] 
      assert newo[i]==wsize or newo[i] <i+2 
      # note that because of the first assertion in this block i+2<wsize
      outbuffer.encode(0 if newo[i]==wsize else newo[i],i+2,Order)
    # --- output logskewed encoded delta value 
    if newo_type[i]==0: # minimal
      assert isinstance(delta[i],int)
      assert delta[i]>0    # we save bits encoding delta[i]-1
      outbuffer.encode(delta[i]-1,max_negdelta,Minimal)
      ## was: outbuffer.encode(delta[i],max_negdelta+1,Minimal)
    elif newo_type[i]==2: # maximal
      assert isinstance(delta[i],int)
      outbuffer.encode(delta[i],max_posdelta+1,Maximal)  
    else:  # intermediate
      assert newo_type[i]==1
      assert isinstance(delta[i],tuple)
      outbuffer.encode(delta[i][0],delta[i][1],Intermediate)
  return 12 + outbuffer.flush()

# compress a single block in strict mode 
def compress_block_strict(values,order,wsize,outf,encoder):
  newo = []
  newo_type = []  # 0: minimal, 1: intermediate, 2: maximal 4: to be ignored 
  delta = []
  buffer = [values[0]]
  for v in values[1:]:
    # code for v is the position of the predecessor in the list
    # we scan the list right to left (new to old) 
    code = 1     # means predecessor not yet found (v is the smallest) 
    succval = v  # means successor of v not yet found  
    assert 0 < len(buffer)<wsize, "Illegal buffer"
    for i in range(1,len(buffer)+1):
      if buffer[-i]== v:
        code = 2*i
        break      # the closest equal value is the predecessor  
      if buffer[-i] < v and (code==1 or buffer[-i]>buffer[-(code//2)]):
        code = 2*i+1
      elif buffer[-i] > v and (succval==v or buffer[-i]<succval): 
        succval = buffer[-i]  
     # --- code and succval computed           
    assert 0 < code< 2* wsize, "Illegal code computation"
    assert code <= 255, "Illegal code value (not a uint8)"  
    assert succval >= v, "Illegal successor computation"    
    assert code%2==0 or code==1 or succval==v or succval > buffer[-(code//2)]
    newo.append(code)    
    if code%2==0:  # consider only odd codes (even codes have no delta value)
      delta.append(-1)   
      newo_type.append(4)   # to be ignored value
    elif code==1:           # minimal value
      delta.append(min(buffer) - v)
      newo_type.append(0)
    elif succval == v:      # maximal value  
      delta.append(v-buffer[-(code//2)])
      newo_type.append(2)     
    else:                   # intermediate
      delta.append((v-buffer[-(code//2)],succval-buffer[-(code//2)])) 
      assert v>buffer[-(code//2)] and v < succval
      newo_type.append(1)
    # update buffer inserting new value as last element
    buffer.append(v)
    if len(buffer)>=wsize:
      assert len(buffer)==wsize
      buffer.pop(0) # remove first (==oldest) element                         
  # --- delta values computed. now get maxpos_delta and maxneg_delta
  assert len(delta)==len(newo_type) and  len(delta)==len(newo)
  assert len(delta) + 1 == len(order) 
  max_posdelta = max([delta[i] for i in range(len(delta)) if newo_type[i]==2]+[0])
  max_negdelta = max([delta[i] for i in range(len(delta)) if newo_type[i]==0]+[0])
  # --- output block header
  outf.write(struct.pack('<i',values[0]))          # first value
  outf.write(struct.pack('<I',max_posdelta))  
  outf.write(struct.pack('<I',max_negdelta))
  # --- output "missing" ordering values  
  outbuffer = mybits.BitBufferOut(outf,encoder)
  for i in range(len(newo)):
    if newo[i]!=order[i+1]:
      # check i and order[i] that cannot be he minimum and must point the small window
      assert i<wsize-2, "Error 1 at order %d: %d vs %d" % (i, newo[i],order[i+1]) # from position wsize-2 it is newo[i]==order[i+1]  
      assert order[i+1]!=1 and (order[i+1]//2)>i+1, "Error 2 at order %d: %d vs %d" % (i, newo[i],order[i+1])
      # new[i] cannot point to an equal value 
      assert newo[i]%2==1, "Error 2 at order %d: %d vs %d" % (i, newo[i],order[i+1])
      # newo[i] can be 1 (the minimum) or point to an entry in [1..i+1] 
      assert newo[i]==1 or (newo[i]//2) <i+2 
      # note that because of the first assertion in this block i+2<wsize
      outbuffer.encode(newo[i]//2,i+2,Order)
    # --- output logskewed encoded delta value 
    if newo_type[i]==0: # minimal
      assert isinstance(delta[i],int)
      assert delta[i]>0
      outbuffer.encode(delta[i]-1,max_negdelta,Minimal)
    elif newo_type[i]==2: # maximal
      assert isinstance(delta[i],int)
      assert delta[i]>0
      outbuffer.encode(delta[i]-1,max_posdelta,Maximal)  
    elif newo_type[i]==1:  # intermediate
      assert isinstance(delta[i],tuple)
      assert delta[i][0]>0
      outbuffer.encode(delta[i][0]-1,delta[i][1]-1,Intermediate)
    else:  # delta value to be ignored
      assert newo_type[i]==4  
      
  return 12 + outbuffer.flush()


# main decompression routine
def decompress(ifile,ofile,outfile,wsize,bsize,decoder):
  with open(ofile,"rb") as of:
    with open(ifile,"rb") as df:
      with open(outfile,"wb") as outf:
        # read num_blocks and block len
        num_blocks = struct.unpack('<i',df.read(4))[0]
        block_lens = []
        for i in range(num_blocks):
          block_lens.append(struct.unpack('<H',df.read(2))[0])
        # decompress one_block at a time
        tot=0
        for i in range(num_blocks):
          if num_blocks< 10 or i%(num_blocks//10)==0: 
            print("Decompressing block", i,file=sys.stderr)
          if Strict_mode:
            values = decompress_block_strict(i,block_lens,of,df,wsize,bsize,decoder)
          else:
            values = decompress_block(i,block_lens,of,df,wsize,bsize,decoder)
          tot += len(values)
          for v in values:
            outf.write(struct.pack('<i',v))  
  return tot


# decompress block n 
def decompress_block_strict(n,block_lens,of,df,wsize,bsize,decoder):
  # --- seek of to the position at the beginning of the block+1
  of.seek(bsize*n+1)  # recall the first order value of a block is discarded
  # --- seek df to the position at the beginning of the block
  assert n < len(block_lens)
  offset = 4 + 2*len(block_lens) + sum(block_lens[0:n])
  df.seek(offset)
  # --- read first value, maxneg maxpos
  v = struct.unpack('<i',df.read(4))[0]
  max_posdelta = struct.unpack('<I',df.read(4))[0]
  max_negdelta = struct.unpack('<I',df.read(4))[0]
  # init buffer of recent values and decompress block
  block = [v]  
  buffer = [v]
  # go on reading from the second value onward
  i=1
  inbuffer = mybits.BitBufferInput(df,decoder) #init bitbuffer
  while len(block) < bsize:
    z = of.read(1)
    if len(z)==0: break  # end of (last) block
    o = struct.unpack('B',z)[0]
    # o is the global order value for next entry 
    assert 0 < o < 2*wsize, "Illegal order value: %d vs %d (strict)" % (o, wsize)
    if o//2>i: # global order out of window, get local order from file 
      o = inbuffer.decode(i+1,Order)
      assert o <= i, "order decoder has serious problems: %d vs %d (strict)" % (o,i+1)
      o = 2*o+ 1
      assert 0 < o < 2*wsize, "Illegal order value (2): %d vs %d (strict)" % (o, wsize)
    # --- now we have the order value relative to the current buffer
    assert o//2 <= len(buffer), "Order value outside buffer (strict)"
    if o%2==0:
      v = buffer[-(o//2)]   # nothing to read from the compressed file
    elif o==1:              # this is a type 0 minimal entry  
      delta = 1+inbuffer.decode(max_negdelta,Minimal)
      # assert delta>0
      v = min(buffer) - delta
    else:
      pred = buffer[-(o//2)]
      if pred == max(buffer): #type 2: maximal
        delta = 1+inbuffer.decode(max_posdelta,Maximal)
      else:                   # type 1: intermediate
        succ = min([i for i in buffer if i>pred])
        delta = 1 + inbuffer.decode(succ - pred-1,Intermediate)
      v = pred + delta
    i += 1
    # --- next value computed, add to block and update buffer
    block.append(v)
    buffer.append(v)
    if len(buffer)>= wsize:
      assert len(buffer)==wsize
      buffer.pop(0)  # remove first (==oldest) element
  inbuffer.logical_flush()    
  return block

# decompress block n 
def decompress_block(n,block_lens,of,df,wsize,bsize,decoder):
  # --- seek of to the position at the beginning of the block+1
  of.seek(bsize*n+1)  # recall the first order value of a block is discarded
  # --- seek df to the position at the beginning of the block
  assert n < len(block_lens)
  offset = 4 + 2*len(block_lens) + sum(block_lens[0:n])
  df.seek(offset)
  # --- read first value, maxneg maxpos
  v = struct.unpack('<i',df.read(4))[0]
  max_posdelta = struct.unpack('<I',df.read(4))[0]
  max_negdelta = struct.unpack('<I',df.read(4))[0]
  # init buffer of recent values and decompressed block
  block = [v]  
  buffer = [v]
  # go on reading from the second value onward
  i=1
  inbuffer = mybits.BitBufferInput(df,decoder) #init bitbuffer
  while len(block) < bsize:
    z = of.read(1)
    if len(z)==0: break  # end of (last) block
    o = struct.unpack('B',z)[0]
    # o is the global order value for next entry 
    assert 0 < o <= wsize, "Illegal order value: %d vs %d" % (o, wsize)
    if o!=wsize and o>i: # this is a missing entry
      o = inbuffer.decode(i+1,Order)
      assert o <= i, "order decoder has serious problems: %d" % i+1
      if o==0: o = wsize
    # --- now we have the order value relative to the current buffer
    if o==wsize: # this is a type 0 minimal entry  
      delta = 1+inbuffer.decode(max_negdelta,Minimal)
      ## was: delta = inbuffer.decode(max_negdelta+1,Minimal)
      v = min(buffer) - delta
    else:
      assert o <= len(buffer)
      pred = buffer[-o]
      if pred == max(buffer): #type 2 maximal
        delta = inbuffer.decode(max_posdelta+1,Maximal)
        v = pred + delta
      else:      # type 1 intermediate
        succ = min([i for i in buffer if i>pred])
        delta = inbuffer.decode(succ - pred,Intermediate)
        v = pred + delta
    i += 1
    # --- next value computed, add to block and update buffer
    block.append(v)
    buffer.append(v)
    if len(buffer)>= wsize:
      assert len(buffer)==wsize
      buffer.pop(0)  # remove first (==oldest) element
  inbuffer.logical_flush()    
  return block
  

    

def main():
  # show_command_line(sys.stderr)
  global Strict_mode
  parser = argparse.ArgumentParser(description=Description, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('infile', help='input file', type=str)
  parser.add_argument('orfile', help='order file', type=str)
  parser.add_argument('--block_size', '-b', help='size of a block, only for compression (def 64)', default=64, type=int)  
  parser.add_argument('--encoder', '-e', help='encoder: lsk|dlt (def lsk)', default='lsk', type=str)  
  parser.add_argument('--decompress', '-d', help='decompress', action='store_true')
  parser.add_argument('--strict', help='(de)compress in strict mode', action='store_true')
  args = parser.parse_args()  
  Strict_mode = args.strict  # in strict mode we care about equal values
  # run the algorithm   
  s1 = time.time()
  ifile = args.infile
  ofile = args.orfile
  if Strict_mode:
    assert ofile.endswith(".yo"), "Invalid extension for order file"
  else:
    assert ofile.endswith(".xo"), "Invalid extension for order file"
  wsize = int(ofile.split(".")[-2])
  tsize = os.path.getsize(ofile)
  outcome = 0  # 0 means no error
  extension = "ez" if Strict_mode else "dz"
  if args.decompress:
    outfile = ifile + "d"
    bsize = int(ifile.split(".")[-2])
    encoder = ifile.split(".")[-4]
    assert encoder in Encoders, "Unknown encoder: " + encoder   
    size = decompress(ifile,ofile,outfile,wsize,bsize, Encoders[encoder]['d'])
    print("Output file %s   Size: %d   Expected: %d" % (outfile,size,tsize),file=sys.stderr)  
    if size!=tsize:
      outcome=1
  else:  # compression 
    assert os.path.getsize(ifile) == 4*tsize
    assert args.encoder in Encoders, "Unknown encoder: " + args.encoder
    bsize = args.block_size
    outfile = ifile + ".%s.%d.%d.%s" % (args.encoder,wsize,bsize,extension)    # output file
    csize = compress(ifile,ofile,outfile,tsize,wsize,bsize,Encoders[args.encoder]['e'])
    print("Output file %s   Size: %d   Compression: %f bits/byte" %
         (outfile,csize,8*csize/(4*tsize)),file=sys.stderr)
  e1 = time.time()
  print("Elapsed time: %.3f\n" % (e1-s1),file=sys.stderr)
  exit(outcome)

  
if __name__ == '__main__':
    main()
 
